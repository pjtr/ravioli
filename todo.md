This is the short term TODO list for the next release.
All long term TODO's should go on the [wiki](https://bitbucket.org/uiterlix/ravioli/wiki/Home).
Bugs that (for some strange reason) won't be fixed in the next release should go in the bug tracker.
This list should be empty when releasing a new version.

* bug: loading an xml that was saved from another model, leads to exceptions
* loading large xml file could use some user feedback, e.g. progressbar...
* unselecting a component makes it move to the bottom of the list; maybe related to custom labels?
* added newly added libraries to about box
