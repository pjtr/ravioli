# Ravioli

## Overview

**Ravioli** - _no spaghetti_ -

Ravioli (previously known as DependencyVisualise), is a graphical visualisation tool for OSGi bundle and service dependencies.

![Ravioli](https://bitbucket.org/uiterlix/ravioli/wiki/ravioli.png)

## Status

Currently, only visualisation of (static) bundle dependencies is implemented and the only available UI is a Java Swing interface. To run the tool, you must deploy it as a bundle in an OSGi container, see below for details. Although the tool is functional, it might have some raw edges, as the project just started. Also, the UI is pretty basic (some might call it ugly), but it works. Even in this stage, all feedback is welcome!

## Requirements
### Functional requirements
- Graphical visualisation of (static) bundle and (dynamic) service dependencies. Bundle dependencies are calculated from both Require-Bundle and Import-Package manifest headers. Service dependencies are obtained from the Components registered with the Apache Felix DependencyManager. 
- Allow the users to re-arrange the nodes in the dependency graph to their liking.
- Allow removal of nodes from the graph.
- Save/Open of dependency graphs.
- Export dependency graph to printable format (PDF/PNG).
- Choice of graph depth when inspecting a node.
- Add incoming/outgoing depedencies to graph function for a node.
- Option to inspect a bundle's manifest headers.
- Create projections by grouping nodes together into a (named) logical node for which the dependencies are determined by the participants of the group.  

### Non functionals
- Fit for enterprise size applications. Applications with thousands of runtime components should be no problem; neither for determining the dependencies nor for visualizing them.

## Building
Gradle is used as build tool. Execute the following command to build and test:

    gradle assemble check

## Deploy
For deploying the bundle in an OSGi container, you'll need a scala library bundle too. Download scala from <http://www.scala-lang.org/download/2.10.3.html> and deploy the `scala-library` as bundle in your OSGi framework. Build or download the `dependencyvisualise-0.1.jar` bundle and deploy it; as soon as the bundle starts, a window will appear that displays the (static) bundle dependencies between all bundles that are present in the OSGi container.

## Development
Please refer to the [Development notes](https://bitbucket.org/uiterlix/ravioli/src/master/Development.md) if you want to know more about the development environment.


## Feedback 
If you run into a bug, have problem using Ravioli or have any other suggestion for improvement, feel free to add an issue at <https://bitbucket.org/uiterlix/ravioli/issues> or post on our [Google group](https://groups.google.com/forum/#!forum/ravioli-forum). Thanks for your cooperation.

## License
This Project is Licensed under LGPL.