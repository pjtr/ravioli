/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * 
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.impl.graph;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.zip.ZipException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.ToolTipManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.TableColumnModel;

import org.jgraph.JGraph;
import org.jgraph.event.GraphSelectionEvent;
import org.jgraph.event.GraphSelectionListener;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphCell;
import org.osgi.framework.BundleContext;
import org.servicedev.depvis.Constants;
import org.servicedev.depvis.Launcher;
import org.servicedev.depvis.impl.graph.model.CompositeGraphComponent;
import org.servicedev.depvis.impl.graph.model.GraphComponent;
import org.servicedev.depvis.impl.graph.model.GraphDependency;
import org.servicedev.depvis.impl.graph.model.GraphModel;
import org.servicedev.depvis.model.Component;
import org.servicedev.depvis.model.ComponentModel;
import org.servicedev.depvis.model.NullModel;
import org.servicedev.depvis.model.base.DefaultComponentModel;
import org.servicedev.depvis.model.bundle.DirectoryScanningManifestComponentModel;
import org.servicedev.depvis.model.bundle.InContainerBundleScanner;

import com.jgraph.layout.JGraphFacade;
import com.jgraph.layout.JGraphLayout;
import com.jgraph.layout.graph.JGraphSimpleLayout;
import com.jgraph.layout.organic.JGraphFastOrganicLayout;
import com.jgraph.layout.simple.SimpleGridLayout;
import org.servicedev.depvis.model.jar.ClassFileScanner;
import org.servicedev.depvis.model.jar.JarFileScanner;
import org.servicedev.depvis.model.jar.PackageComponentModel;

/**
 * JFrame representing the Graph application.
 */
public class GraphFrame extends JFrame {

    private GraphModel model;
    private final boolean standalone;
    private JGraphModelAdapter jGraphModelAdapter;
	private JGraph jGraph;
	private JGraphLayoutMorphingManager morpher = new JGraphLayoutMorphingManager();
	private JSplitPane splitPane;
	private JTable selectionTable;
	private GraphTableModel selectionTableModel;
	private JScrollPane graphScrollPane;
    private boolean sideBarVisible = true;
    private int dividerSize;
    private BundleContext bundleContext;
    private RegexBasedLabelConverter labelConverter = new RegexBasedLabelConverter();
    private final boolean debug = Boolean.getBoolean(Constants.DEPVIS_DEBUG_SYSTEM_PROPERTY);

    public GraphFrame() {
        this.standalone = true;
        this.model = new GraphModel(new NullModel(), null);
        labelConverter.setRegexReplacements(LabelPrefsDialog.getLabelReplacementPrefs());
        createUI();
    }

    public GraphFrame(BundleContext bundleContext) {
        this.standalone = false;
        this.bundleContext = bundleContext;
        labelConverter.setRegexReplacements(LabelPrefsDialog.getLabelReplacementPrefs());

        model = new GraphModel(new DefaultComponentModel(), labelConverter);
        createUI();
        loadInContainerBundleDependencies();
    }

    public final void createUI() {
    	setSize(1024,768);
        JMenuBar menubar = new JMenuBar();

        JMenu file = new JMenu("File");
        JMenuItem scan = new JMenuItem("(Re)Scan OSGi Framework");
        scan.setEnabled(bundleContext != null);
        scan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                loadInContainerBundleDependencies();
            }
        });
        file.add(scan);
        JMenuItem manifest = new JMenuItem("Scan bundle manifests...");
        manifest.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                File manifestDir = selectDirectory();
                if (manifestDir != null) {
                    loadManifestBundleDependencies(manifestDir);
                }
            }
        });
        file.add(manifest);
        JMenuItem jar = new JMenuItem("Analyse jar file...");
        jar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                File jarFile = selectJarFile();
                if (jarFile != null) {
                    loadJarFilePackageDependencies(jarFile);
                }
            }
        });
        jar.setEnabled(false);  // developed on other branch
        file.add(jar);
        JMenuItem classes = new JMenuItem("Analyse class files...");
        classes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                File classesDir = selectDirectory();
                if (classesDir != null) {
                    loadClassFilePackageDependencies(classesDir);
                }
            }
        });
        file.add(classes);
        classes.setEnabled(false);  // developed on other branch
        file.addSeparator();
        JMenuItem save = new JMenuItem("Save view state...");
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                saveViewState();
            }
        });
        file.add(save);
        JMenuItem load = new JMenuItem("Load view state...");
        load.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                loadViewState();
            }
        });
        file.add(load);
        file.addSeparator();
        JMenuItem exit = new JMenuItem("Exit");
        file.add(exit);
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                exit();
            }
        });
        menubar.add(file);

        JMenu view = new JMenu("View");
        menubar.add(view);
        JCheckBoxMenuItem sideBarSwitch = new JCheckBoxMenuItem("Sidebar", sideBarVisible);
        sideBarSwitch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                toggleSideBar();
            }
        });
        view.add(sideBarSwitch);
        addViewMenuDebugOptions(view);

        JMenu prefs = new JMenu("Preferences");
        JMenuItem labelPrefs = new JMenuItem("Labels...");
        labelPrefs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                LabelPrefsDialog dlg = new LabelPrefsDialog(GraphFrame.this);
                dlg.setVisible(true);
                labelConverter.setRegexReplacements(dlg.getReplacements());
                jGraphModelAdapter.refreshNodes();
            }
        });
        prefs.add(labelPrefs);
        menubar.add(prefs);
        JMenu help = new JMenu("Help");
        JMenuItem about = new JMenuItem("About " + Launcher.getName() + "...");
        about.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                showAbout();
            }
        });
        help.add(about);
        menubar.add(help);
        setJMenuBar(menubar);
        
        // TODO: Add items for model selection

        JToolBar toolbar = new JToolBar();

        JButton exitButton = new JButton("Exit");
        toolbar.add(exitButton);
        exitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                exit();
            }

        });
        
        JButton fdLayoutButton = new JButton("Force directed layout");
        toolbar.add(fdLayoutButton);
        fdLayoutButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                applyForceDirectedLayout();
            }

        });
        
        JButton circleLayoutButton = new JButton("Circle layout");
        toolbar.add(circleLayoutButton);
        circleLayoutButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                applyCircleLayout();
            }

        });
        
        JButton gridLayoutButton = new JButton("Grid layout");
        toolbar.add(gridLayoutButton);
        gridLayoutButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                applyGridLayout();
            }

        });
        
        JButton defaultZoomButton = new JButton("0");
        toolbar.add(defaultZoomButton);
        defaultZoomButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                jGraph.setScale(1.0);
            }

        });
        
        JButton zoomInButton = new JButton("+");
        toolbar.add(zoomInButton);
        zoomInButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                jGraph.setScale(jGraph.getScale() * 2);
            }

        });
        
        JButton zoomOutButton = new JButton("-");
        toolbar.add(zoomOutButton);
        zoomOutButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                jGraph.setScale(jGraph.getScale() / 2);
            }

        });

        JButton groupButton = new JButton("group");
        toolbar.add(groupButton);
        groupButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                int messageType = JOptionPane.INFORMATION_MESSAGE;
                String compositeIdentifier = JOptionPane.showInputDialog(GraphFrame.this, 
                   "Composite identifier?", 
                   "Create composite component", messageType);
                if (compositeIdentifier != null && !compositeIdentifier.trim().equals("")) {  // Value is null when dialog is cancelled
                    if (!model.containsIdentifier(compositeIdentifier))
                        groupCurrentSelection(compositeIdentifier);
                    else
                        JOptionPane.showConfirmDialog(GraphFrame.this, "Identifier '" + compositeIdentifier + "' is already used in this model.", "Error", JOptionPane.DEFAULT_OPTION);
                }
                else if (compositeIdentifier != null)
                    JOptionPane.showConfirmDialog(GraphFrame.this, "Group identifier can't be empty.", "Error", JOptionPane.DEFAULT_OPTION);
            }
        });
        
        JButton unGroupButton = new JButton("ungroup");
        toolbar.add(unGroupButton);
        unGroupButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                unGroupCurrentSelection();
            }
        });

        JButton toggleSelectionButton = new JButton("toggle");
        toolbar.add(toggleSelectionButton);
        toggleSelectionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                toggleSelection();
            }
        });
        
        JButton expandOutgoingDependenciesButton = new JButton("expand out");
        toolbar.add(expandOutgoingDependenciesButton);
        expandOutgoingDependenciesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                expandOutgoingDependenciesForSelection();
            }
        });      

        JButton expandIncomingDependenciesButton = new JButton("expand in");
        toolbar.add(expandIncomingDependenciesButton);
        expandIncomingDependenciesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                expandIncomingDependenciesForSelection();
            }
        });  

        addToolbarDebugOptions(toolbar);
        add(toolbar, BorderLayout.NORTH);

        setTitle(Launcher.getExtendedName());
        setLocationRelativeTo(null);
        setDefaultCloseOperation(standalone? EXIT_ON_CLOSE: DISPOSE_ON_CLOSE);
        splitPane = new JSplitPane();
        getContentPane().add(splitPane);
        JComponent graphPanel = createGraphPanel();
        JComponent selectionPanel = createSelectionTablePanel();
        splitPane.setLeftComponent(graphPanel);
        splitPane.setRightComponent(selectionPanel);
        splitPane.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent componentEvent) {
                if (sideBarVisible) {
                    splitPane.setDividerLocation(computeDividerLocation());
                }
            }
        });
        
        // graph selection listener
        jGraph.addGraphSelectionListener(new GraphSelectionListener() {
			
			@Override
			public void valueChanged(GraphSelectionEvent e) {
				handleGraphSelectionChanged();
			}

		});
        
        selectionTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                handleTableSelectionChanged();
            }
        });
    }

    private void saveViewState() {
        JFileChooser fileSelector = new JFileChooser();
        if (JFileChooser.APPROVE_OPTION == fileSelector.showSaveDialog(this)) {
            model.save(fileSelector.getSelectedFile());
        }
    }

    private void loadViewState() {
        JFileChooser fileSelector = new JFileChooser();
        if (JFileChooser.APPROVE_OPTION == fileSelector.showOpenDialog(this)) {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            long start = System.currentTimeMillis();
            model.load(fileSelector.getSelectedFile());
            refreshModels();
            setCursor(Cursor.getDefaultCursor());
            if (debug) {
                System.out.println("Loading '" + fileSelector.getSelectedFile() + "' took " + (System.currentTimeMillis() - start) + " ms.");
            }
        }
    }

    private File selectJarFile() {
        JFileChooser fileSelector = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("jar file", "jar");
        fileSelector.setFileFilter(filter);
        if (JFileChooser.APPROVE_OPTION == fileSelector.showOpenDialog(this)) {
            return fileSelector.getSelectedFile();
        }
        return null;
    }

    private File selectDirectory() {
        JFileChooser fileSelector = new JFileChooser();
        fileSelector.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (JFileChooser.APPROVE_OPTION == fileSelector.showOpenDialog(this)) {
            return fileSelector.getSelectedFile();
        }
        return null;
    }

    private void addToolbarDebugOptions(JToolBar toolbar) {
    	if (Boolean.getBoolean(Constants.DEPVIS_DEBUG_SYSTEM_PROPERTY)) {
	        JButton debugButton = new JButton("debug");
	        toolbar.add(debugButton);
	        debugButton.addActionListener(new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent actionEvent) {
	                debugSelection();
	            }
	        });	
    	}

        JButton test1 = new JButton("test dir");
        toolbar.add(test1);
        test1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                loadClassFilePackageDependencies(new File("/Users/peter/Documents"));
            }
        });
        JButton test2 = new JButton("test jar");
        toolbar.add(test2);
        test2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                loadJarFilePackageDependencies(new File("/Users/peter/lib/osgi/org.apache.felix.http.bundle-2.2.1.jar"));
            }
        });
    }
    
    private void addViewMenuDebugOptions(JMenu menu) {
    	if (Boolean.getBoolean(Constants.DEPVIS_DEBUG_SYSTEM_PROPERTY)) {
	        JMenuItem refresh = new JMenuItem("Refresh");
	        refresh.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    refreshModels();
                }
            });
	        menu.add(refresh);
    	}
    }

    private void exit() {
        if (standalone) {
            System.exit(0);
        }
        else {
            GraphFrame.this.setVisible(false);
            GraphFrame.this.dispose();
        }
    }

    private void refreshModels() {
    	jGraphModelAdapter.refresh();
        selectionTableModel.refresh();
    }

    private void toggleSideBar() {
        if (sideBarVisible) {
            dividerSize = splitPane.getDividerSize();
            splitPane.setDividerSize(0);
            splitPane.setDividerLocation(1.0);
            splitPane.getRightComponent().setVisible(false);
        }
        else {
            splitPane.setDividerSize(dividerSize);
            splitPane.setDividerLocation(computeDividerLocation());
            splitPane.getRightComponent().setVisible(true);
        }
        sideBarVisible = !sideBarVisible;
    }

    private int computeDividerLocation() {
        int rightSize = splitPane.getRightComponent().getPreferredSize().width;
        int width = splitPane.getWidth();
        return width - rightSize;
    }

    @Override
    public void setVisible(boolean visible) {
    	super.setVisible(visible);
        if (visible) {
            splitPane.setDividerLocation(computeDividerLocation());
    	}
    }
    
	private void applyForceDirectedLayout() {
		JGraphFastOrganicLayout layout = new JGraphFastOrganicLayout();
		layout.setForceConstant(200);
		applyAutoLayout(layout);
	}
	
	private void applyCircleLayout() {
		JGraphSimpleLayout layout = new JGraphSimpleLayout(JGraphSimpleLayout.TYPE_CIRCLE, 1024, 768);
		applyAutoLayout(layout);
	}
	
	private void applyGridLayout() {
		SimpleGridLayout layout = new SimpleGridLayout();
		applyAutoLayout(layout);
	}
	
	private void applyAutoLayout(JGraphLayout layout) {
		JGraphFacade graphFacade = new JGraphFacade(jGraph);
		layout.run(graphFacade);
		Map nestedMap = graphFacade.createNestedMap(true, true);
		morpher.morph(jGraph, nestedMap);	
	}

    private JComponent createGraphPanel() {
        jGraphModelAdapter = new JGraphModelAdapter(model);
        jGraph = new JGraph(jGraphModelAdapter) {
            public String getToolTipText(MouseEvent e) {
                Object cell = jGraph.getFirstCellForLocation(e.getX(), e.getY());
                if (cell instanceof DefaultEdge) {
                    Object userObject = ((DefaultEdge) cell).getUserObject();
                    String tooltip = ((GraphDependency) userObject).getExplanation();
                    return tooltip;
                }
                return null;
            }
        };
        ToolTipManager.sharedInstance().registerComponent(jGraph);

		graphScrollPane = new JScrollPane(jGraph);
        return graphScrollPane;
	}
    
    private JComponent createSelectionTablePanel() {
    	selectionTableModel = new GraphTableModel(model);
    	selectionTable = new JTable(selectionTableModel);
    	selectionTable.setAutoCreateRowSorter(true);
    	selectionTable.setGridColor(Color.BLACK);
    	TableColumnModel columnModel = selectionTable.getColumnModel();
    	columnModel.getColumn(0).setPreferredWidth(20);
		columnModel.getColumn(1).setPreferredWidth(120);
    	columnModel.getColumn(2).setPreferredWidth(40);
    	columnModel.getColumn(3).setPreferredWidth(30);
    	JScrollPane scrollPane = new JScrollPane(selectionTable);
    	selectionTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        scrollPane.setPreferredSize(new Dimension(227, 600));
		return scrollPane;
    }
    
	private void handleGraphSelectionChanged() {
		List<GraphComponent> selectedComponents = new ArrayList<GraphComponent>();
		int[] currentSelectedRows = selectionTable.getSelectedRows();
		// convert to Integer[] for easy comparison
		List<Integer> currentSelectedRowList = new ArrayList<Integer>();
		for (int row : currentSelectedRows) {
			currentSelectedRowList.add(row);
		}
		List<Integer> newSelectedRows = new ArrayList<Integer>();
		for (Object cell : jGraph.getSelectionCells() ) {
			if (cell instanceof DefaultGraphCell) {
				DefaultGraphCell graphCell = (DefaultGraphCell) cell;
				Object userObject = graphCell.getUserObject();
				if (userObject instanceof GraphComponent) {
					selectedComponents.add((GraphComponent)userObject);
					int modelRowIndex = selectionTableModel.getRow((GraphComponent)userObject);
					try {
						newSelectedRows.add(selectionTable.convertRowIndexToView(modelRowIndex));
					} catch (IndexOutOfBoundsException aie) {
						// table model is out of sync TODO: Find a better way to handle this
						selectedComponents.clear();
						break;
					}
					
				}
			}
		}
		if (!currentSelectedRowList.equals(newSelectedRows)) {
			ListSelectionModel selectionModel = selectionTable.getSelectionModel();
			selectionModel.clearSelection();
			for (int rowIndex : newSelectedRows) {
				selectionModel.addSelectionInterval(rowIndex, rowIndex);
			}
		}
	}
	
	private void handleTableSelectionChanged() {
		int[] selectedRows = selectionTable.getSelectedRows();
		List<GraphComponent> selectedComponents = new ArrayList<GraphComponent>();
		for (int rowIndex : selectedRows) {
			int modelRowIndex = selectionTable.convertRowIndexToModel(rowIndex);
			GraphComponent graphComponent = selectionTableModel.getGraphComponent(modelRowIndex);
			selectedComponents.add(graphComponent);
		}
		List<GraphCell> cellsForGraphComponents = jGraphModelAdapter.getCellsForGraphComponents(selectedComponents);
		jGraph.setSelectionCells(cellsForGraphComponents.toArray());
		if (cellsForGraphComponents.size() > 0) {
			GraphCell firstSelectedCell = cellsForGraphComponents.get(0);
			jGraph.scrollCellToVisible(firstSelectedCell);
		}
	}
	
	private void toggleSelection() {
		int[] selectedRows = selectionTable.getSelectedRows();
		for (int rowIndex : selectedRows) {
			int modelRowIndex = selectionTable.convertRowIndexToModel(rowIndex);
			GraphComponent graphComponent = selectionTableModel.getGraphComponent(modelRowIndex);
			graphComponent.setVisible(!graphComponent.isVisible());
		}
	}
	
	private void expandOutgoingDependenciesForSelection() {
		int[] selectedRows = selectionTable.getSelectedRows();
		for (int rowIndex : selectedRows) {
			int modelRowIndex = selectionTable.convertRowIndexToModel(rowIndex);
			GraphComponent graphComponent = selectionTableModel.getGraphComponent(modelRowIndex);
			for (GraphDependency dependency : graphComponent.getOutgoingDependencies()) {
				dependency.getTargetComponent().setVisible(true);
			}
		}		
	}
	
	private void expandIncomingDependenciesForSelection() {
		int[] selectedRows = selectionTable.getSelectedRows();
		for (int rowIndex : selectedRows) {
			int modelRowIndex = selectionTable.convertRowIndexToModel(rowIndex);
			GraphComponent graphComponent = selectionTableModel.getGraphComponent(modelRowIndex);
			for (GraphDependency dependency : graphComponent.getIncomingDependencies()) {
				dependency.getSourceComponent().setVisible(true);
			}
		}		
	}

    private void debugSelection() {
        for (GraphComponent component: getSelectedComponents()) {
            System.out.println("Component " + component + ":");
            System.out.println("Out:");
            for (GraphDependency dep: component.getOutgoingDependencies()) {
                System.out.println("- " + dep.getTargetComponent() + " (" + dep.getExplanation() + ")");
            }
            System.out.println("In:");
            for (GraphDependency dep: component.getIncomingDependencies()) {
                System.out.println("- " + dep.getSourceComponent() + " (" + dep.getExplanation() + ")");
            }
        }
    }

    private List<GraphComponent> getSelectedComponents() {
        int[] selectedRows = selectionTable.getSelectedRows();
        List<GraphComponent> selectedComponents = new ArrayList<GraphComponent>();
        for (int rowIndex : selectedRows) {
            int modelRowIndex = selectionTable.convertRowIndexToModel(rowIndex);
            selectedComponents.add(selectionTableModel.getGraphComponent(modelRowIndex));
        }
        return selectedComponents;
    }

    /**
     * Converts current selection of components into a group, actually a CompositeGraphComponent
     * @param groupIdentifier        the identifier of the new group
     */
	private void groupCurrentSelection(String groupIdentifier) {
        // Gather the selected graph components
		int[] selectedRows = selectionTable.getSelectedRows();
		List<GraphComponent> selectedComponents = new ArrayList<GraphComponent>();
		for (int rowIndex : selectedRows) {
			int modelRowIndex = selectionTable.convertRowIndexToModel(rowIndex);
			GraphComponent graphComponent = selectionTableModel.getGraphComponent(modelRowIndex);
			if (!(graphComponent instanceof CompositeGraphComponent)) {
				selectedComponents.add(graphComponent);
			}
		}
        // And group them
		if (selectedComponents.size() > 0) {    // TODO (discuss): creating groups of size 1 does not make much sense, does it?
            GraphComponent groupedComponent = model.group(selectedComponents, groupIdentifier);
            // select the group
            List<GraphCell> cellsForGraphComponents = jGraphModelAdapter.getCellsForGraphComponents(Collections.singletonList(groupedComponent));
            jGraph.setSelectionCells(cellsForGraphComponents.toArray());
        }
	}

    /**
     * Converts currently selected groups in their members, i.e. "ungroups" them.
     */
	private void unGroupCurrentSelection() {
		int[] selectedRows = selectionTable.getSelectedRows();
		List<GraphComponent> selectedComponents = new ArrayList<GraphComponent>();
		for (int rowIndex : selectedRows) {
			int modelRowIndex = selectionTable.convertRowIndexToModel(rowIndex);
			GraphComponent graphComponent = selectionTableModel.getGraphComponent(modelRowIndex);
			selectedComponents.add(graphComponent);
		}
		// ungroup selected components
        List<GraphComponent> ungroupedComponents = new ArrayList<GraphComponent>();
		for (GraphComponent graphComponent : selectedComponents) {
			if (graphComponent instanceof CompositeGraphComponent) {
                ungroupedComponents.addAll(model.ungroup((CompositeGraphComponent) graphComponent));
			}
		}
        // select the components that are ungrouped
        List<GraphCell> cellsForGraphComponents = jGraphModelAdapter.getCellsForGraphComponents(ungroupedComponents);
        jGraph.setSelectionCells(cellsForGraphComponents.toArray());

    }

    private void showAbout() {
        String programName = Launcher.getName();

        String aboutText = "<html><h3>" + programName + " " + Launcher.getVersion() + "</h3>" +
                "<p>" + programName + " is open source and licensed under LGPL.<br>" +
                "Included libraries:<br>" +
                "- JGraph, see https://github.com/jgraph/jgraphx <br>" +
                "- MigLayout, see http://www.miglayout.com/" +
                "</p>" +
                "<p style=\"padding-top:10px;\">For more info see https://bitbucket.org/uiterlix/ravioli.<p>" +
                "</html>";
        JOptionPane.showMessageDialog(this, new JLabel(aboutText), "About " + programName, JOptionPane.INFORMATION_MESSAGE);
    }

    private void loadInContainerBundleDependencies() {
        assert bundleContext != null;

        LoadTask task = new LoadTask() {
            protected ComponentModel getComponentModel() {
                return new InContainerBundleScanner(bundleContext, this);
            }
        };
        task.execute();
    }

    public void loadManifestBundleDependencies(final File dir) {
        LoadTask task = new LoadTask() {
            protected ComponentModel getComponentModel() {
                return new DirectoryScanningManifestComponentModel(dir, this);
            }
        };
        task.execute();
    }

    public void loadJarFilePackageDependencies(final File jarFile) {
        LoadTask task = new LoadTask() {
            protected ComponentModel getComponentModel() {
                return new PackageComponentModel(new JarFileScanner(jarFile, this));
            }
        };
        task.execute();
    }

    public void loadClassFilePackageDependencies(final File classesDir) {
        LoadTask task = new LoadTask() {
            protected ComponentModel getComponentModel() {
                return new PackageComponentModel(new ClassFileScanner(classesDir, this));
            }
        };
        task.execute();
    }

    public void loadModel(final ComponentModel model) {
        LoadTask task = new LoadTask() {
            protected ComponentModel getComponentModel() {
                return model;
            }
        };
        task.execute();
    }

    private abstract class LoadTask extends SwingWorker<GraphModel, String> implements Progress {

        private final String taskTitle;
        private long start;
        private long end;

        private JDialog progressDlg;
        private final JProgressBar progressBar = new JProgressBar();
        private final JLabel progressText = new JLabel();

        private int progressCount = 0;

        public LoadTask() {
            this.taskTitle = "Creating model";
            progressBar.setIndeterminate(true);
            setProgressText("Starting...");
        }

        @Override
        public void setRange(final int min, final int max) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    progressBar.setIndeterminate(false);
                    progressBar.setMinimum(min);
                    progressBar.setMaximum(max);

                }
            });
        }

        @Override
        public void increment() {
            progressCount++;
            publish(""); // null causes Exception in SwingWorker
        }

        @Override
        public void setCurrentProgress(int progress) {
            progressCount = progress;
            publish(""); // null causes Exception in SwingWorker
        }

        @Override
        public void setProgressText(String message) {
            publish(message);
        }

        @Override
        public void setIndeterminate(boolean on) {
            progressBar.setIndeterminate(on);
        }

        @Override
        public boolean cancelled() {
            // Progress.cancelled is delegated to SwingWorker.isCancelled
            return isCancelled();
        }

        // Note that if this method does not send any progress, the progress dialog is not shown at all.
        // The same holds if progress is only send at the beginning (during the first 100 ms, see below).
        abstract protected ComponentModel getComponentModel();

        @Override
        protected GraphModel doInBackground() throws Exception {
            start = System.currentTimeMillis();
            GraphModel newModel = new GraphModel(getComponentModel(), labelConverter);
            setProgressText("Preparing model...");
            newModel.prepare();
            setProgressText("Updating graph...");
            return newModel;
        }

        @Override
        protected void process(List<String> messages) {
            // Only show progress after some time
            if ((System.currentTimeMillis() - start) > 100) {
                if (progressDlg == null) {
                    // This might seem strange, as this method is called on the EDT, but when invoke directly, the
                    // call will block on the setVisible of the modal dialog and this call will thus not return. In
                    // some cases (when the worker finishes very fast), this will lead to a progress dialog that
                    // does not close when done.
                    // Strange enough, this does not happen always (as you would expect); it seems the EDT is handling
                    // long blocking modal dialogs in a 'smarter' way...
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            showProgressDialog();
                        }
                    });
                }
                progressBar.setValue(progressCount);
            }
            // Find latest (non-empty) message
            for (int i = messages.size() - 1; i >= 0; i--) {
                String current = messages.get(i);
                if (current != null && current.length() > 0) {
                    progressText.setText(current);
                    break;
                }
            }
        }

        @Override
        protected void done() {
            if (debug)
                System.out.println("creating model took " + (System.currentTimeMillis() - start) +  " ms");  // TODO: use logger
            long startUI = System.currentTimeMillis();
            if (! isCancelled()) {
                try {
                    model = get();

                    long models = System.currentTimeMillis();
                    jGraphModelAdapter = new JGraphModelAdapter(model);
                    if (debug)
                        System.out.println("creating JGraphModel took " + (System.currentTimeMillis()-models) + " ms");
                    jGraph.setModel(jGraphModelAdapter);
                    graphScrollPane.setViewportView(jGraph);

                    selectionTableModel.setModel(model);
                    selectionTableModel.refresh();

                    applyForceDirectedLayout();

                } catch (InterruptedException e) {
                    // (thrown by SwingWorker.get()) - Impossible: we're done
                } catch (ExecutionException e) {
                    if (e.getCause() instanceof ZipException)
                        JOptionPane.showMessageDialog(GraphFrame.this, "Invalid jar file.", "Error", JOptionPane.ERROR_MESSAGE);
                    else {
                        if (debug) {
                            System.err.println("An error occurred during loading: " + e);
                            e.printStackTrace();
                        }
                        JOptionPane.showMessageDialog(GraphFrame.this, "A surprising new error has occurred.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
            end = System.currentTimeMillis();
            if (progressDlg != null && end - start < 500) {
                // Avoid flashing, display progress dialog a little longer
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {}
            }

            if (progressDlg != null) {
                progressDlg.setVisible(false);
                progressDlg.dispose();
            }

            long endUI = System.currentTimeMillis();
            if (debug) {
                System.out.println("creating UI took " + (endUI - startUI) + " ms");
                System.out.println("Loading took " + (end-start) + " ms");
            }

            String result = model.getComponentModel().getCreationResult();
            if (result != null && result.trim().length() > 0)
                JOptionPane.showMessageDialog(GraphFrame.this, result, "result", JOptionPane.OK_OPTION);
        }

        private void showProgressDialog() {
            if (! isDone()) {
                progressDlg = new JDialog(GraphFrame.this, taskTitle, true);
                progressDlg.setLocationRelativeTo(GraphFrame.this);

                JPanel panel = new JPanel();
                panel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
                BoxLayout boxLayout = new BoxLayout(panel, BoxLayout.PAGE_AXIS);
                panel.setLayout(boxLayout);

                panel.add(progressBar);
                progressText.setPreferredSize(new Dimension(200, progressText.getPreferredSize().height));
                progressText.setAlignmentX(0.5f);
                panel.add(progressText);

                JButton cancelButton = new JButton("cancel");
                cancelButton.setAlignmentX(0.5f);
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        cancel(true);
                    }
                });
                panel.add(cancelButton);

                progressDlg.add(panel);
                progressDlg.pack();
                progressDlg.setVisible(true);
            }
        }
    }
}
