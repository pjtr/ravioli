/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 * <p/>
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 * <p/>
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * <p/>
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.impl.graph;

/**
 * Converts labels to strings that are better human readable.
 */
public interface LabelConverter {
    String convert(String label);
}
