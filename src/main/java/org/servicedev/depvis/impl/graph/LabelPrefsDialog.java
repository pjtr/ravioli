/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 * <p/>
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 * <p/>
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * <p/>
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.impl.graph;

import net.miginfocom.swing.MigLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Preferences dialog for configuring label replacements.
 */
public class LabelPrefsDialog extends JDialog {

    public static String helpText = "Enter a regular expression together with a replacement pattern for each label your want to replace. " +
            "For example, the pair 'org\\.apache\\.(.*)', '$1' will strip 'org.apache' from the names.";

    private final JPanel regexPanel;
    private final JButton moreButton;
    private List<JTextField> regexs = new ArrayList<JTextField>();
    private List<JTextField> replacements = new ArrayList<JTextField>();
    private int nrOfStoredPrefs;

    public LabelPrefsDialog(JFrame frame) {
        super(frame, true);

        regexPanel = new JPanel(new MigLayout());
        regexPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(9, 9, 9, 9), BorderFactory.createEtchedBorder()));
        regexPanel.add(new JLabel("<html>" + helpText + "</html>"), "span, wrap, wmax 90%");
        nrOfStoredPrefs = loadPrefs();
        if (nrOfStoredPrefs == 0) {
            addLine();
        }
        addLine();
        moreButton = new JButton("More");
        moreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                regexPanel.remove(moreButton);
                addLine();
                regexPanel.add(moreButton);
                LabelPrefsDialog.this.pack();

            }
        });
        regexPanel.add(moreButton, "wrap");

        JPanel bottom = new JPanel();
        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String error = validateInput();
                if (error != null) {
                    JOptionPane.showMessageDialog(LabelPrefsDialog.this, error, "Error", JOptionPane.DEFAULT_OPTION);
                }
                else {
                    savePrefs();
                    setVisible(false);
                }
            }
        });
        bottom.add(okButton);
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(regexPanel, BorderLayout.CENTER);
        panel.add(bottom, BorderLayout.SOUTH);

        setContentPane(panel);
        getRootPane().setDefaultButton(okButton);
        pack();
    }

    public List<String> getReplacements() {
        List<String> result = new ArrayList<String>();

        for (int i = 0; i < regexs.size(); i++) {
            String regex = regexs.get(i).getText().trim();
            String replacement = replacements.get(i).getText().trim();
            if (! regex.equals("") && ! replacement.equals("")) {
                result.add(regex);
                result.add(replacement);
            }
        }

        return result;
    }

    private void addLine() {
        regexPanel.add(new JLabel("Match regex:"));
        JTextField regexField = new JTextField(20);
        regexField.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent focusEvent) {
                focusEvent.getComponent().setForeground(Color.BLACK);
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                JTextField field = (JTextField) focusEvent.getComponent();
                if (! validRegex(field.getText())) {
                    field.setForeground(Color.RED);
                }
            }
        });
        regexs.add(regexField);
        regexPanel.add(regexField);
        regexPanel.add(new JLabel("Replacement pattern:"));
        JTextField replaceField = new JTextField(10);
        replacements.add(replaceField);
        regexPanel.add(replaceField, "wrap");
    }

    private String validateInput() {

        for (int i = 0; i < regexs.size(); i++) {
            if (regexs.get(i).getText().trim().equals("") && !replacements.get(i).getText().trim().equals("") ||
                    ! regexs.get(i).getText().trim().equals("") && replacements.get(i).getText().trim().equals(""))
            return "Each regex must have a matching replacement and vice versa.";
        }
        for (JTextField regex: regexs) {
            if (! validRegex(regex.getText())) {
                return "Invalid regular expression.";
            }
        }
        return null;
    }

    private boolean validRegex(String regex) {
        try {
            Pattern.compile(regex);
            return true;
        } catch (PatternSyntaxException invalid) {
            return false;
        }
    }

    private void savePrefs() {
        Preferences prefs = Preferences.userNodeForPackage(GraphFrame.class);
        List<String> replacements = getReplacements();
        int i;
        for (i = 0; i < replacements.size() / 2; i++) {
            prefs.put("labelConverterRegex" + i, replacements.get(2*i));
            prefs.put("labelConverterReplacement" + i, replacements.get(2*i+1));
        }
        for (; i < nrOfStoredPrefs; i++) {
            prefs.remove("labelConverterRegex" + i);
            prefs.remove("labelConverterReplacement" + i);
        }
    }

    private int loadPrefs() {
        Preferences prefs = Preferences.userNodeForPackage(GraphFrame.class);
        int i;
        for (i = 0; i < 100; i++) {
            String regex = prefs.get("labelConverterRegex" + i, null);
            String replacement = prefs.get("labelConverterReplacement" + i, null);
            if (regex != null && replacement != null) {
                addLine();
                regexs.get(i).setText(regex);
                replacements.get(i).setText(replacement);
            }
            else
                break;
        }
        return i;
    }

    public static List<String> getLabelReplacementPrefs() {
        List<String> result = new ArrayList<String>();

        Preferences prefs = Preferences.userNodeForPackage(GraphFrame.class);
        int i;
        for (i = 0; i < 100; i++) {
            String regex = prefs.get("labelConverterRegex" + i, null);
            String replacement = prefs.get("labelConverterReplacement" + i, null);
            if (regex != null && replacement != null) {
                result.add(regex);
                result.add(replacement);
            }
            else
                break;
        }
        return result;
    }

    public static void main(String[] args) {
        JDialog dlg = new LabelPrefsDialog(null);
        dlg.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        dlg.setVisible(true);
    }
}
