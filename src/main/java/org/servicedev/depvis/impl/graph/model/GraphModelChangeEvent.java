/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * 
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.impl.graph.model;

/**
 * Change event describing a graph model change. 
 */
public class GraphModelChangeEvent {

	public enum ChangeType { 
		COMPONENT_ADD, 
		COMPONENT_REMOVE, 
		COMPONENT_VISIBILITY_CHANGE, 
		LOCATION_CHANGE,
		DEPENDENCY_ADD,
		DEPENDENCY_REMOVE,
		GRAPH_RESET }
	
	private ChangeType type;
	private GraphComponent graphComponent;
	private GraphDependency graphDependency;
	
	public GraphModelChangeEvent(ChangeType type, GraphComponent component) {
		this.type = type;
		this.graphComponent = component;
	}
	
	public GraphModelChangeEvent(ChangeType type, GraphDependency dependency) {
		this.type = type;
		this.graphDependency = dependency;
	}
	
	public GraphModelChangeEvent(ChangeType type) {
		this.type = type;
	}
	
	/**
	 * Returns the type of change.
	 * @return
	 */
	public ChangeType getType() {
		return type;
	}
	
	/**
	 * Returns the {@link GraphComponent} that was the actual subject of the change event.
	 * @return
	 */
	public GraphComponent getGraphComponent() {
		return graphComponent;
	}
	
	/**
	 * Returns the {@link GraphDependency} that was the actual subject of the change event.
	 * @return
	 */
	public GraphDependency getGraphDependency() {
		return graphDependency;
	}
	
}
