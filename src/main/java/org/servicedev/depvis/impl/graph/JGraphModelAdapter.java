/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * 
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.impl.graph;

import java.awt.Color;
import java.awt.Font;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.BorderFactory;

import org.jgraph.event.GraphModelEvent;
import org.jgraph.event.GraphModelListener;
import org.jgraph.graph.AttributeMap;
import org.jgraph.graph.AttributeMap.SerializableRectangle2D;
import org.jgraph.graph.ConnectionSet;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.DefaultGraphModel;
import org.jgraph.graph.DefaultPort;
import org.jgraph.graph.GraphCell;
import org.jgraph.graph.GraphConstants;
import org.servicedev.depvis.impl.graph.model.CompositeGraphComponent;
import org.servicedev.depvis.impl.graph.model.GraphComponent;
import org.servicedev.depvis.impl.graph.model.GraphDependency;
import org.servicedev.depvis.impl.graph.model.GraphModelChangeEvent;
import org.servicedev.depvis.impl.graph.model.GraphModelChangeEvent.ChangeType;
import org.servicedev.depvis.impl.graph.model.GraphModelChangeListener;

/**
 * Adapter that wraps a GraphModel into a JGraphModel. 
 */
public class JGraphModelAdapter extends DefaultGraphModel implements GraphModelChangeListener, GraphModelListener {

    public static final Color NODE_COLOR =       Color.decode("#FFA500");
    public static final Color GROUP_NODE_COLOR = Color.decode("#D2691E");
    public static final Color EDGE_COLOR =       Color.decode("#7AA1E6");

    private org.servicedev.depvis.impl.graph.model.GraphModel graphModel;
	private Map<String, DefaultGraphCell> nodeMap = new HashMap<String, DefaultGraphCell>();
	// Local cache of graph components to used to detect what modifications have been made to the graph model
	private List<GraphComponent> cachedGraphComponents = new ArrayList<GraphComponent>();
	
	public JGraphModelAdapter(org.servicedev.depvis.impl.graph.model.GraphModel graphModel) {
		this.graphModel = graphModel;
		graphModel.addGraphModelChangeListener(this);
		addGraphModelListener(this);
		// create nodes and vertices based on graph model
		updateComponents();
	}

    public void refreshNodes() {
        cellsChanged(nodeMap.values().toArray());
    }

	private void reset() {
		// remove all cells and vertices from graph
		for (DefaultGraphCell cell : nodeMap.values()) {
			Set edges = getEdges(this, new Object[] { cell });
			remove(edges.toArray());
			remove(new Object[] { cell });
		}
		// flush administration
		nodeMap.clear();
		cachedGraphComponents.clear();
	}
	
	private void addJGraphNode(GraphComponent component) {
		if (!nodeMap.containsKey(component.getIdentifier())) {
			DefaultGraphCell cell = new DefaultGraphCell(component);
			cell.add(new DefaultPort());
			cell.setUserObject(component);
			AttributeMap attributeMap = new AttributeMap();
			attributeMap.put(cell, getNodeAttributes(cell));
			insert(new Object[] { cell }, attributeMap, null, null, null);
			nodeMap.put(component.getIdentifier(), cell);
		}
	}
	
	private void addJGraphNodes(Collection<GraphComponent> components) {
		Set<DefaultGraphCell> cells = new HashSet<DefaultGraphCell>();
		AttributeMap attributeMap = new AttributeMap();
		for (GraphComponent component : components) {
			if (!nodeMap.containsKey(component.getIdentifier())) {
				DefaultGraphCell cell = new DefaultGraphCell(component);
				cell.add(new DefaultPort());
				cell.setUserObject(component);
				nodeMap.put(component.getIdentifier(), cell);
				cells.add(cell);
				attributeMap.put(cell, getNodeAttributes(cell));
			}
		}
		insert(cells.toArray(), attributeMap, null, null, null);
	}

	private void addJGraphEdge(GraphDependency dependency) {
		ConnectionSet set = new ConnectionSet();
		DefaultEdge theEdge = new DefaultEdge(dependency);
		DefaultGraphCell from = null;
		DefaultGraphCell to = null;
		GraphComponent fromComponent = dependency.getSourceComponent();
		String fromVertexId = fromComponent.getIdentifier();
		GraphComponent toComponent = dependency.getTargetComponent();
		String toVertexId = toComponent.getIdentifier();
		if (!nodeMap.containsKey(fromVertexId)) {
			addJGraphNode(fromComponent);
		}
		from = nodeMap.get(fromVertexId);

		if (!nodeMap.containsKey(toVertexId)) {
			addJGraphNode(toComponent);
		}

		to = nodeMap.get(toVertexId);
		
		set.connect(theEdge, (DefaultPort) from.getChildAt(0),
				(DefaultPort) to.getChildAt(0));
		AttributeMap edgeAttributeMap = new AttributeMap();
		edgeAttributeMap.put(theEdge, getEdgeAttributes(theEdge));
		insert(new Object[] { theEdge }, edgeAttributeMap, set, null,
				null);
	}
	
	private void addJGraphEdges(Collection<GraphDependency> dependencies) {
		Set<DefaultEdge> edgesToAdd = new HashSet<DefaultEdge>();
		AttributeMap edgeAttributeMap = new AttributeMap();
		ConnectionSet set = new ConnectionSet();
		for (GraphDependency dependency : dependencies) {
			DefaultEdge theEdge = new DefaultEdge(dependency);
			DefaultGraphCell from = null;
			DefaultGraphCell to = null;
			GraphComponent fromComponent = dependency.getSourceComponent();
			String fromVertexId = fromComponent.getIdentifier();
			GraphComponent toComponent = dependency.getTargetComponent();
			String toVertexId = toComponent.getIdentifier();
			if (!nodeMap.containsKey(fromVertexId)) {
				addJGraphNode(fromComponent);
			}
			from = nodeMap.get(fromVertexId);
	
			if (!nodeMap.containsKey(toVertexId)) {
				addJGraphNode(toComponent);
			}
	
			to = nodeMap.get(toVertexId);
			
			set.connect(theEdge, (DefaultPort) from.getChildAt(0),
					(DefaultPort) to.getChildAt(0));
			edgeAttributeMap.put(theEdge, getEdgeAttributes(theEdge));
			edgesToAdd.add(theEdge);
		}
		insert(new Object[] { edgesToAdd.toArray() }, edgeAttributeMap, set, null,
				null);
	}
	

	private AttributeMap getEdgeAttributes(DefaultEdge edge) {
		AttributeMap eMap = new AttributeMap();
        GraphConstants.setDisconnectable(eMap, false);
		GraphConstants.setLineEnd(eMap, GraphConstants.ARROW_TECHNICAL);
		GraphConstants.setEndFill(eMap, true);
		GraphConstants.setEndSize(eMap, 10);
		GraphConstants.setForeground(eMap, Color.decode("#25507C"));
		GraphConstants.setFont(eMap, GraphConstants.DEFAULTFONT.deriveFont(Font.BOLD, 12));
		GraphConstants.setLineColor(eMap, EDGE_COLOR);
		return eMap;
	}

	private AttributeMap getNodeAttributes(DefaultGraphCell cell) {
		AttributeMap vMap = new AttributeMap();

		GraphComponent graphComponent = (GraphComponent) cell.getUserObject();
		if (graphComponent.getXPosition() == 0 && graphComponent.getYPosition() == 0) {
			GraphConstants.setBounds(vMap, new Rectangle2D.Double(50, 50, 140, 30));
		} else {
			double x = graphComponent.getXPosition();
			double y = graphComponent.getYPosition();
			GraphConstants.setBounds(vMap, new Rectangle2D.Double(x, y, graphComponent.getWidth(), graphComponent.getHeight()));
		}
		GraphConstants.setBorder(vMap, BorderFactory.createRaisedBevelBorder());
		if (graphComponent instanceof CompositeGraphComponent) {
			GraphConstants.setBackground(vMap, GROUP_NODE_COLOR);
		} else {
			GraphConstants.setBackground(vMap, NODE_COLOR);
		}
		GraphConstants.setForeground(vMap, Color.white);
		GraphConstants.setFont(vMap, GraphConstants.DEFAULTFONT.deriveFont(Font.BOLD, 12));
		GraphConstants.setOpaque(vMap, true);
        GraphConstants.setEditable(vMap, false);
        GraphConstants.setSizeableAxis(vMap, GraphConstants.X_AXIS);

        return vMap;
	}

	@Override
	public void onGraphModelChange(GraphModelChangeEvent event) {
		if (event.getType() == ChangeType.COMPONENT_VISIBILITY_CHANGE) {
			GraphComponent graphComponent = event.getGraphComponent();
			if (!graphComponent.isVisible()) {
				// hide
				removeGraphNode(graphComponent);
			} else if (!nodeMap.containsKey(graphComponent.getIdentifier())) {
				// show
				addGraphNode(graphComponent);
			}
		}
		if (event.getType() == ChangeType.COMPONENT_ADD ||
				event.getType() == ChangeType.COMPONENT_REMOVE) {
			updateComponents();
		}
		if (event.getType() == ChangeType.DEPENDENCY_ADD ||
				event.getType() == ChangeType.DEPENDENCY_REMOVE) {
			updateDependency(event.getType(), event.getGraphDependency());
		}
		if (event.getType() == ChangeType.GRAPH_RESET) {
			refresh();
		}
	}
	
	private void updateComponents() {
		// diff the current graphmodel components with those in the cached graph components
		List<GraphComponent> graphComponentsToRemove = new ArrayList<GraphComponent>();
		for (GraphComponent graphComponent : cachedGraphComponents) {
			if (graphModel.getGraphComponent(graphComponent.getIdentifier()) == null) {
				// remove from graphComponents list
				graphComponentsToRemove.add(graphComponent);
			}
		}
		for (GraphComponent graphComponent : graphComponentsToRemove) {
			if (nodeMap.containsKey(graphComponent.getIdentifier())) {
				removeGraphNode(graphComponent);
				cachedGraphComponents.remove(graphComponent);			
			}
		}

		Set<GraphComponent> componentsToAdd = new HashSet<GraphComponent>();
		// added components will always be drawn
		for (GraphComponent graphComponent : graphModel.getGraphComponents()) {
			if (!cachedGraphComponents.contains(graphComponent)) {
				if (graphComponent.isVisible()) {
					componentsToAdd.add(graphComponent);
				}
				cachedGraphComponents.add(graphComponent);
			}
		}
		addGraphNodes(componentsToAdd);
	}
	
	private void updateDependency(ChangeType changeType, GraphDependency dependency) {
		if (changeType == ChangeType.DEPENDENCY_REMOVE) {
			// remove the dependency
			DefaultGraphCell sourceComponentCell = nodeMap.get(dependency.getSourceComponent().getIdentifier());
			DefaultGraphCell targetComponentCell = nodeMap.get(dependency.getSourceComponent().getIdentifier());
			if (sourceComponentCell != null && targetComponentCell != null) {
			Set edges = getEdges(this, new Object[] { sourceComponentCell });
				for (Object edge : edges) {
					DefaultGraphCell sourceCell = (DefaultGraphCell)((DefaultPort)((DefaultEdge)edge).getSource()).getParent();
					DefaultGraphCell targetCell = (DefaultGraphCell)((DefaultPort)((DefaultEdge)edge).getTarget()).getParent();
					if (sourceCell == sourceComponentCell
							&& targetCell == targetComponentCell) {
						// matches our dependency
						remove(new Object[] { edge });
					}
				}
			}
		} else if (changeType == ChangeType.DEPENDENCY_ADD) {
			// add the dependency
			addJGraphEdge(dependency);
		}
	}
	
	private void addGraphNode(GraphComponent graphComponent) {
		addJGraphNode(graphComponent);
		for (GraphDependency dependency : graphComponent.getOutgoingDependencies()) {
			// only add the edge if the target is actually being displayed
			String targetIdentifier = dependency.getTargetComponent().getIdentifier();
			if (nodeMap.containsKey(targetIdentifier)) {
				addJGraphEdge(dependency);
			}
		}
		for (GraphDependency dependency : graphComponent.getIncomingDependencies()) {
			// only add the edge if the source is actually being displayed
			// TODO: Map to grouped components
			String sourceIdentifier = dependency.getSourceComponent().getIdentifier();
			if (nodeMap.containsKey(sourceIdentifier)) {
				addJGraphEdge(dependency);
			}
		}
	}
	
	private void addGraphNodes(Collection<GraphComponent> components) {
		addJGraphNodes(components);
		Set<GraphDependency> edgesToAdd = new HashSet<GraphDependency>();
		for (GraphComponent graphComponent : components) {
			for (GraphDependency dependency : graphComponent.getOutgoingDependencies()) {
				// only add the edge if the target is actually being displayed
				String targetIdentifier = dependency.getTargetComponent().getIdentifier();
				if (nodeMap.containsKey(targetIdentifier)) {
					edgesToAdd.add(dependency);
				}
			}
			for (GraphDependency dependency : graphComponent.getIncomingDependencies()) {
				// only add the edge if the source is actually being displayed
				// TODO: Map to grouped components
				String sourceIdentifier = dependency.getSourceComponent().getIdentifier();
				if (nodeMap.containsKey(sourceIdentifier)) {
					edgesToAdd.add(dependency);
				}
			}
		}
		for (GraphDependency dependency : edgesToAdd) {
			addJGraphEdge(dependency);
		}
	}
	
	private void removeGraphNode(GraphComponent graphComponent) {
		DefaultGraphCell cell = nodeMap.get(graphComponent.getIdentifier());
		Set edges = getEdges(this, new Object[] { cell });
		remove(edges.toArray());
		remove(new Object[] { cell });
		nodeMap.remove(graphComponent.getIdentifier());
	}

	@Override
	public void graphChanged(GraphModelEvent e) {
		Object source = e.getSource();
		for (Object object : e.getChange().getChanged()) {
			if (e.getChange().getRemoved() != null && Arrays.asList(e.getChange().getRemoved()).contains(object)) {
				// not interested in a removed event here
				continue;
			}
			Object[] inserted = e.getChange().getInserted();
			if (inserted != null && Arrays.asList(e.getChange().getInserted()).contains(object)) {
				// not interested in a inserted event here
				continue;
			}
			if (object instanceof DefaultGraphCell) {
				DefaultGraphCell cell = (DefaultGraphCell) object;
				if (cell.getUserObject() instanceof GraphComponent) {
					SerializableRectangle2D bounds = (SerializableRectangle2D) cell.getAttributes().get("bounds");
					((GraphComponent)cell.getUserObject()).setBounds(bounds.x, bounds.y, bounds.width, bounds.height);
				}
			}
		}
		
	}
	
	public List<GraphCell> getCellsForGraphComponents(Collection<GraphComponent> graphComponents) {
		List<GraphCell> cells = new ArrayList<GraphCell>();
		for (GraphComponent component : graphComponents) {
			if (nodeMap.containsKey(component.getIdentifier())) {
				cells.add(nodeMap.get(component.getIdentifier()));
			}
		}
		return cells;
	}

	public void refresh() {
		// rather brute force, just remove and recreate all jgraph cells and vertices
		reset();
		updateComponents();
	}
	
}
