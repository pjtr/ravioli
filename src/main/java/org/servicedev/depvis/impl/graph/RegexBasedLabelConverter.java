/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 * <p/>
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 * <p/>
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * <p/>
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.impl.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A label converter that converts label based on (regex, replacement) pairs.
 */
public class RegexBasedLabelConverter implements LabelConverter {

    private List<Pattern> patterns = new ArrayList<Pattern>();
    private List<String> replacements = new ArrayList<String>();

    public void setRegexReplacements(List<String> regexReplacements) {
        patterns.clear();
        replacements.clear();
        for (int i = 0; i < regexReplacements.size(); i += 2) {
            patterns.add(Pattern.compile(regexReplacements.get(i)));
            replacements.add(regexReplacements.get(i + 1));
        }
    }

    @Override
    public String convert(String label) {
        return convert(label, false);
    }

    private String convert(String label, boolean escapeIllegalDollarExpressions) {
        for (int i = 0; i < patterns.size(); i++) {
            Matcher m = patterns.get(i).matcher(label);
            if (m.matches()) {
                StringBuffer buffer = new StringBuffer();
                String replacement = escapeIllegalDollarExpressions? escapeGroupRefsLargerThen(m.groupCount(), replacements.get(i)): replacements.get(i);
                try {
                    m.appendReplacement(buffer, replacement);
                }
                catch (IndexOutOfBoundsException noSuchGroup) {
                    assert escapeIllegalDollarExpressions == false;
                    // We've run into a nasty replacement string with a group ref to a group that does not exist; rerun with extensive checking
                    return convert(label, true);
                }
                catch (IllegalArgumentException incorrectDollar) {
                    assert escapeIllegalDollarExpressions == false;
                    // We've run into a nasty replacement string, with illegal dollar refs like $no; rerun with extensive checking
                    return convert(label, true);
                }
                m.appendTail(buffer);
                return buffer.toString();
            }
        }
        return label;
    }

    private String escapeGroupRefsLargerThen(int groupCount, String string) {
        Matcher m = Pattern.compile("\\$\\d*").matcher(string);
        StringBuffer buffer = new StringBuffer();
        while (m.find()) {
            int index = -1;
            if (m.group().length() > 1)
                index = Integer.parseInt(m.group().substring(1));
            if (index >= 0 && index <= groupCount) {
                // An ok group reference. Escape the dollar just once for the appendReplacement method, it will end up as a normal dollar
                m.appendReplacement(buffer, "\\$" + index);
            }
            else if (index < 0) {
                // An illegal group reference: dollar not followed by number
                // Double-escape the dollar, it will end up as \$ in the string. The six slashes are needed because
                // four slashes means an escaped \, which we need to escape again.
                m.appendReplacement(buffer, "\\\\\\$");
            }
            else {
                // Group reference to a group that does not exist.
                // Double-escape the dollar, it will end up as \$ in the string. The six slashes are needed because
                // four slashes means an escaped \, which we need to escape again.
                m.appendReplacement(buffer, "\\\\\\$" + index);
            }
        }
        m.appendTail(buffer);
        return buffer.toString();
    }

}
