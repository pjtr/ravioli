/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.impl.graph.model;

import java.util.List;

import scala.xml.Elem;

/**
 * A component in the <code>GraphModel</code>.
 */
public interface GraphComponent {

	boolean isVisible();

    String getLabel();

	String getIdentifier();

	List<GraphDependency> getOutgoingDependencies();

	List<GraphDependency> getIncomingDependencies();

    /**
     * Adds an incoming dependency, if it's not already there.
     * @param dependency
     * @return  the dependency that is added, or if it is not added because it matched an existing, that existing (that might be modified).
     */
    GraphDependency addIncomingDependency(GraphDependency dependency);

    /**
     * Adds an outgoing dependency, if it's not already there.
     * @param dependency
     * @return  the dependency that is added, or if it is not added because it matched an existing, that existing (that might be modified).
     */
    GraphDependency addOutgoingDependency(GraphDependency dependency);

    void removeIncomingDependency( GraphDependency dependency);

    void removeOutgoingDependency(GraphDependency dependency);

    void setVisible(boolean visible);

	double getXPosition();

	double getYPosition();

	double getWidth();

	double getHeight();

	void setBounds(double x, double y, double width, double height);

	Elem toXml();
}
