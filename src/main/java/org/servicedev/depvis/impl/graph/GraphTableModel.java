/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * 
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.impl.graph;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.servicedev.depvis.impl.graph.model.GraphComponent;
import org.servicedev.depvis.impl.graph.model.GraphModel;
import org.servicedev.depvis.impl.graph.model.GraphModelChangeEvent;
import org.servicedev.depvis.impl.graph.model.GraphModelChangeEvent.ChangeType;
import org.servicedev.depvis.impl.graph.model.GraphModelChangeListener;


public class GraphTableModel extends AbstractTableModel implements GraphModelChangeListener {

	private GraphModel graphModel;
	private static String[] headers = new String[] { "On", "Component", "# out", "# in" };
	private List<GraphComponent> cachedGraphComponents = new ArrayList<GraphComponent>();

	public GraphTableModel(GraphModel graphModel) {
		this.graphModel = graphModel;
		graphModel.addGraphModelChangeListener(this);
		cachedGraphComponents.addAll(graphModel.getGraphComponents());
	}
	
	public void setModel(GraphModel graphModel) {
		graphModel.removeGraphModelChangeListener(this);
		this.graphModel = graphModel;
		this.graphModel.addGraphModelChangeListener(this);
		reset();
	}

    public void reset() {
    	cachedGraphComponents.clear();
        cachedGraphComponents.addAll(graphModel.getGraphComponents());
    }

	@Override
	public String getColumnName(int column) {
		return headers[column];
	}
	
	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public int getRowCount() {
		return cachedGraphComponents.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		GraphComponent component = cachedGraphComponents.get(row);
		switch (col) {
			case 0:
				return component.isVisible();
			case 1:
				return component.getLabel();
			case 2:
				return component.getOutgoingDependencies().size();
			case 3: 
				return component.getIncomingDependencies().size();
			default:
				break;
		} 
		throw new IllegalStateException("Unexpected column index.");
	}
	
	@Override
	public Class<?> getColumnClass(int col) {
		switch (col) {
		case 0:
			return Boolean.class;
		case 1:
			return String.class;
		case 2:
			return Integer.class;
		case 3: 
			return Integer.class;
		default:
			break;
		}
		throw new IllegalStateException("Unexpected column index.");
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		boolean editable = columnIndex == 0;
		return editable;
	}

	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		super.setValueAt(value, rowIndex, columnIndex);
		if (columnIndex == 0) {
			GraphComponent graphComponent = cachedGraphComponents.get(rowIndex);
			graphComponent.setVisible(!graphComponent.isVisible());
		}
	}
	
	public int getRow(GraphComponent component) {
		return cachedGraphComponents.indexOf(component);
	}
	
	public GraphComponent getGraphComponent(int row) {
		return cachedGraphComponents.get(row);
	}

	@Override
	public void onGraphModelChange(GraphModelChangeEvent event) {
		if (event.getType() == ChangeType.GRAPH_RESET) {
			setModel(graphModel);
		} 
		if (event.getType() == ChangeType.COMPONENT_VISIBILITY_CHANGE) {
			fireTableDataChanged();
		}
		if (event.getType() == ChangeType.COMPONENT_ADD ||
				event.getType() == ChangeType.COMPONENT_REMOVE) {
			
			// diff the current graphmodel components with those in the cached graph components
			List<GraphComponent> graphComponentsToRemove = new ArrayList<GraphComponent>();
			for (GraphComponent graphComponent : cachedGraphComponents) {
				if (graphModel.getGraphComponent(graphComponent.getIdentifier()) == null) {
					// remove from graphComponents list
					graphComponentsToRemove.add(graphComponent);
				}
			}
			for (GraphComponent graphComponent : graphComponentsToRemove) {
					cachedGraphComponents.remove(graphComponent);
			}
			

			// added components will always be drawn
			for (GraphComponent graphComponent : graphModel.getGraphComponents()) {
				if (!cachedGraphComponents.contains(graphComponent)) {
					cachedGraphComponents.add(graphComponent);
				}
			}
			refresh();
		}
	}
	
	public void refresh() {
		fireTableDataChanged();
	}
}
