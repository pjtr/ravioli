/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis

import java.util.prefs.Preferences
import org.servicedev.depvis.impl.graph.GraphFrame
import java.lang.{Exception, String}
import javax.swing.SwingUtilities
import org.servicedev.depvis.model.base.{DefaultComponentModel, DefaultDependency, DefaultComponent}
import org.servicedev.depvis.impl.graph.model.GraphModel
import org.servicedev.depvis.model.ComponentModel
import scala.collection.JavaConversions._
import java.io.File
import scala.collection.mutable.ListBuffer

object Launcher extends App {

  val options = parseCommandLineArgs(args.toList, Map())
  
  if (options.contains('debug)) {
    println("Enabling debug mode")
    System.setProperty(Constants.DEPVIS_DEBUG_SYSTEM_PROPERTY, "true")
  }

  if (options.contains('help)) {
    help()
    sys.exit()
  }

  if (options.contains('clearPrefs))
    clearPrefs()

  var initialAction: (GraphFrame) => Unit = null

  if (options.contains('demo)) {
    val demoModel = createDemo(options('demo))
    initialAction = (app: GraphFrame) => app.loadModel(demoModel);
  }

  if (options.contains('manifest)) {
    val dir = new File(options('manifest))
    initialAction = (app: GraphFrame) => app.loadManifestBundleDependencies(dir)
  }

  if (options.contains('bytecode)) {
    val jarFile = new File(options('bytecode))
    initialAction = (app: GraphFrame) => app.loadJarFilePackageDependencies(jarFile)
  }

  if (options.contains('classes)) {
    val classesDir = new File(options('classes))
    initialAction = (app: GraphFrame) => app.loadClassFilePackageDependencies(classesDir)
  }

  val app: GraphFrame = new GraphFrame();
  app.setVisible(true)
  if (initialAction != null)
    initialAction(app)

  def parseCommandLineArgs(argList: List[String], args: Map[Symbol, String]): Map[Symbol, String] = {
    argList match {
      case Nil => args
      case ("-h" | "--help")    :: tail => parseCommandLineArgs(tail, args ++ Map('help -> null))
      case "--demo" :: demo     :: tail => parseCommandLineArgs(tail, args ++ Map('demo -> demo))
      case "--clearPrefs"       :: tail => parseCommandLineArgs(tail, args ++ Map('clearPrefs -> null))
      case "--debug"            :: tail => parseCommandLineArgs(tail, args ++ Map('debug -> null))
      case "--manifests" :: dir :: tail => parseCommandLineArgs(tail, args ++ Map('manifest -> dir))
      case "--jar" :: jar       :: tail => parseCommandLineArgs(tail, args ++ Map('bytecode -> jar))
      case "--classes":: dir    :: tail => parseCommandLineArgs(tail, args ++ Map('classes -> dir))
      case unknown              :: tail => println("Unknown option " + unknown); parseCommandLineArgs(tail, args)
    }
  }

  def help() {
    println(getName() + " version " + getVersion())
    println("To use " + getName() + ", deploy it in an OSGi container.")
    println("You cannot use this version standalone, unless you supply the --demo argument")
  }

  def clearPrefs() {
    val prefs: Preferences = Preferences.userNodeForPackage(classOf[GraphFrame])
    prefs.removeNode()
  }

  def createDemo(demo: String): ComponentModel = {
    var componentModel = new DefaultComponentModel

    if (demo == "xander") {
      val a: DefaultComponent = new DefaultComponent("A")
      val b: DefaultComponent = new DefaultComponent("B")
      val c: DefaultComponent = new DefaultComponent("C")
      val d: DefaultComponent = new DefaultComponent("D")
      val aToB: DefaultDependency = new DefaultDependency(a, b)
      a.addOutgoingDependency(aToB)
      b.addIncomingDependency(aToB)
      val aToC: DefaultDependency = new DefaultDependency(a, c)
      a.addOutgoingDependency(aToC)
      c.addIncomingDependency(aToC)
      val bToD: DefaultDependency = new DefaultDependency(b, d)
      b.addOutgoingDependency(bToD)
      d.addIncomingDependency(bToD)
      componentModel.addComponent(a)
      componentModel.addComponent(b)
      componentModel.addComponent(c)
      componentModel.addComponent(d)

      for (i <- 0 to 30) {
        val id: String = "A " + i
        val component: DefaultComponent = new DefaultComponent(id)
        val componentToC: DefaultDependency = new DefaultDependency(component, c)
        component.addOutgoingDependency(componentToC)
        c.addIncomingDependency(componentToC)
        componentModel.addComponent(component)
      }
    } else if (demo == "performance") {
      val a: DefaultComponent = new DefaultComponent("A")
      val b: DefaultComponent = new DefaultComponent("B")
      val c: DefaultComponent = new DefaultComponent("C")
      val d: DefaultComponent = new DefaultComponent("D")
      val aToB: DefaultDependency = new DefaultDependency(a, b)
      a.addOutgoingDependency(aToB)
      b.addIncomingDependency(aToB)
      val aToC: DefaultDependency = new DefaultDependency(a, c)
      a.addOutgoingDependency(aToC)
      c.addIncomingDependency(aToC)
      val bToD: DefaultDependency = new DefaultDependency(b, d)
      b.addOutgoingDependency(bToD)
      d.addIncomingDependency(bToD)
      componentModel.addComponent(a)
      componentModel.addComponent(b)
      componentModel.addComponent(c)
      componentModel.addComponent(d)

      val aComponents : ListBuffer[DefaultComponent] = new ListBuffer[DefaultComponent]()
      for (i <- 0 to 50) {
        var id: String = "A " + i
        var component: DefaultComponent = new DefaultComponent(id)
        val componentToC: DefaultDependency = new DefaultDependency(component, c)
        component.addOutgoingDependency(componentToC)
        c.addIncomingDependency(componentToC)
        componentModel.addComponent(component)
        aComponents.add(component)
      }
      for (i <- 0 to 500) {
        val id: String = "B " + i
        val component: DefaultComponent = new DefaultComponent(id)
        val cToComponent: DefaultDependency = new DefaultDependency(c, component)
        component.addIncomingDependency(cToComponent)
        c.addOutgoingDependency(cToComponent)
        for (i <- 0 to 50) {
	        val aComponent = aComponents.get(i)
	        val componentToA: DefaultDependency = new DefaultDependency(component, aComponent)
	        component.addOutgoingDependency(componentToA)
	        aComponent.addIncomingDependency(componentToA)
        }
        componentModel.addComponent(component)
      }
    }
    else if (demo == "peter" || true) {
      val a = new DefaultComponent("A")
      val b = new DefaultComponent("B")
      val c = new DefaultComponent("C")
      val d = new DefaultComponent("D")
      val aToC = new DefaultDependency(a, c).addSelf()
      val bToD = new DefaultDependency(b, d).addSelf()
      val cToD = new DefaultDependency(c, d).addSelf()
      componentModel = new DefaultComponentModel(List(a, b, c, d))
    }
    return componentModel
  }

  def getName(): String = "Ravioli"

  def getExtendedName(): String = "Ravioli - no spaghetti"

  def getVersion(): String = {
    // Call version method of generated Version class with reflection, so IDE can still compile this project if Version class not yet generated.
    try {
      val className = this.getClass().getName()
      val mainPackage = className.take(className.lastIndexOf("."))
      val versionClass = Class.forName(mainPackage + ".Version")
      val versionMethod = versionClass.getMethod("getVersion")
      var version = versionMethod.invoke(null).asInstanceOf[String]
      if (! version.matches("\\d.*")) {
        version += " version"
      }
      return version;
    }
    catch {
      case e:Exception => println(e); return "unknown version"
    }
  }

}
