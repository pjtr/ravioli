/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.impl.graph.model

import org.servicedev.depvis.model.{Dependency}
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty

class GraphDependency(@JsonIgnore val source: GraphComponent, @JsonIgnore val target: GraphComponent, @JsonIgnore val model: GraphModel, @JsonIgnore var dependencies: List[Dependency]) {

  @JsonIgnore var internalDeps: Set[Dependency] = Set(dependencies:_*)
  
  // For Json serialization purpose only
  @JsonProperty("source") def getSourceComponentIdentifier() : String = {
    source.getIdentifier()
  }

  // For Json serialization purpose only
  @JsonProperty("target") def getTargetComponentIdentifier() : String = {
    target.getIdentifier()
  }

  def this(source: GraphComponent, target: GraphComponent, model: GraphModel, dependency: Dependency) = this(source, target, model, List(dependency))

  @JsonIgnore def getSourceComponent(): GraphComponent = {
    return source
  }

  @JsonIgnore def getTargetComponent(): GraphComponent = {
    return target
  }

  def merge(dependency: GraphDependency) {
    assert(this.source == dependency.source)
    assert(this.target == dependency.target)
    internalDeps = internalDeps ++ dependency.internalDeps
  }

  @JsonProperty("explanation") def getExplanation(): String = {
    model.getComponentModel().explainDependencies(internalDeps.toList)
  }

  override def toString: String = ""

  override def hashCode: Int = source.hashCode + target.hashCode

  override def equals(other: Any): Boolean = {
    if (other.getClass == classOf[GraphDependency]) {
      val that: GraphDependency = other.asInstanceOf[GraphDependency]
      return (this.source == that.source) && (this.target == that.target)
    }
    else return false
  }

}

object GraphDependency {
  def convert(dependencies: List[GraphDependency]): List[Dependency] = {
    dependencies.map { _.dependencies }.flatten
  }
}