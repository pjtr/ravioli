/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.impl.graph.model;

import org.servicedev.depvis.impl.graph.model.GraphModelChangeEvent.ChangeType
import scala.collection.mutable
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import com.fasterxml.jackson.annotation.JsonIgnore

/**
 * Composite component representing the contained components: dependencies from outside to components inside are moved
 * to the composite and dependencies from inside the composite to outside components are moved to the composite too.
 */
class CompositeGraphComponent(id: String, var members: List[GraphComponent], @JsonIgnore graphModel: GraphModel) extends GraphComponent {

  private var visible = true
  private var x: Double = 0
  private var y: Double = 0
  private var width: Double = 0
  private var height: Double = 0
  private var identifier: String = id

  private var incomingDependencies: ListBuffer[GraphDependency] = computeIncomingDeps().to[ListBuffer]
  private var outgoingDependencies: ListBuffer[GraphDependency] = computeOutgoingDeps().to[ListBuffer]

  private def computeOutgoingDeps(): List[GraphDependency] = {
    // Process outgoing dependencies: collect by target
    val collectedOutgoing = new mutable.HashMap[GraphComponent, mutable.Set[GraphDependency]] with mutable.MultiMap[GraphComponent, GraphDependency]
    members.foreach { _.getOutgoingDependencies.foreach { outgoing =>
      if (! members.contains(outgoing.getTargetComponent))  // Skip internal (member to member) dependencies
        collectedOutgoing.addBinding(outgoing.getTargetComponent, outgoing)
    }}

    // For each for the targets: create a composite dependency
    collectedOutgoing.map { entry =>
      val target = entry._1
      val depsFromMembers = entry._2.toList
      val compositeDep = new GraphDependency(this, target, graphModel, GraphDependency.convert(depsFromMembers))
      // For all outgoing deps (from any member), we need to modify (the incoming deps of) the target: replace existing incoming deps by the composite dep
      depsFromMembers.foreach { depFromMember =>
        target.removeIncomingDependency(depFromMember)
        // Disconnect this dep from the source (which is a member of ours) also; when the member re-appears, the dep may be invalid due to other grouping/ungrouping
        depFromMember.getSourceComponent().removeOutgoingDependency(depFromMember)
      }
      target.addIncomingDependency(compositeDep)
      // Let the map function return the composite dependency
      compositeDep
    }.toList
  }

  private def computeIncomingDeps(): List[GraphDependency] = {
    // Process incoming dependencies of all members: collect by source
    val collectedIncoming = new mutable.HashMap[GraphComponent, mutable.Set[GraphDependency]] with mutable.MultiMap[GraphComponent, GraphDependency]
    members.foreach { _.getIncomingDependencies.foreach { incoming =>
      if (! members.contains(incoming.getSourceComponent))  // Skip internal (member to member) dependencies
        collectedIncoming.addBinding(incoming.getSourceComponent(), incoming)
    }}

    collectedIncoming.map { entry =>
      val source = entry._1
      val depsToMembers = entry._2.toList
      val compositeDep = new GraphDependency(source, this, graphModel, GraphDependency.convert(depsToMembers))
      // For all incoming deps (to any member), we need to modify the source: replace existing outgoing deps by the composite dep
      depsToMembers.foreach { depToMember =>
        source.removeOutgoingDependency(depToMember)
        // Disconnect this dep from the target (which is a member of ours) also; when the member re-appears, the dep may be invalid due to other grouping/ungrouping
        depToMember.getTargetComponent().removeIncomingDependency(depToMember)
      }
      source.addOutgoingDependency(compositeDep)
      // Let the map function return the composite dependency
      compositeDep
    }.toList
  }

  def restoreUnderlyingDependencies() {
    // First, remove all external outgoing: we'll re-add below and there might be outgoing dependencies to components that are currently not in the graph anymore (because of (un)grouping
    members.foreach { member =>
      member.getOutgoingDependencies.foreach { outgoing =>
        // Do not remove internal dependencies; they can't be wrong and if this composite has no external outgoing, nothing is restored.
        if (! members.contains(outgoing.getTargetComponent())) {
          member.removeOutgoingDependency(outgoing)
          outgoing.getTargetComponent().removeIncomingDependency(outgoing)
          graphModel.fireChangeEvent(new GraphModelChangeEvent(GraphModelChangeEvent.ChangeType.DEPENDENCY_REMOVE, outgoing))
        }
      }
    }
    outgoingDependencies.foreach { outgoing => restoreOutgoing(outgoing) }
    incomingDependencies.foreach { incoming => restoreIncoming(incoming) }
  }

  private def restoreOutgoing(outgoing: GraphDependency) {
    assert(outgoing.getSourceComponent == this)
    val target = outgoing.getTargetComponent
    target.removeIncomingDependency(outgoing)   // Note there is no point in removing from the source, because the source (this composite) is discarded anyway
    graphModel.fireChangeEvent(new GraphModelChangeEvent(GraphModelChangeEvent.ChangeType.DEPENDENCY_REMOVE, outgoing))

    members.foreach { member =>
      val componentDeps =
      if (target.isInstanceOf[CompositeGraphComponent]) {
        val targetMembers = target.asInstanceOf[CompositeGraphComponent].getMembers.asScala.toList
        graphModel.findComponentDependencies(member.asInstanceOf[SimpleGraphComponent], targetMembers)
      }
      else {
        graphModel.findComponentDependencies(member.asInstanceOf[SimpleGraphComponent], List(target))
      }
      componentDeps.foreach { dep =>
        val source = graphModel.findById(dep.getSourceComponent.getIdentifier())
        assert(source == member)
        if (target.isInstanceOf[SimpleGraphComponent]) {
          assert (target == graphModel.findById(dep.getTargetComponent.getIdentifier()))
        }
        val restoredDep = new GraphDependency(member, target, graphModel, dep)
        val addOrChangedDep = member.addOutgoingDependency(restoredDep)
        target.addIncomingDependency(restoredDep)
        graphModel.fireChangeEvent(new GraphModelChangeEvent(GraphModelChangeEvent.ChangeType.DEPENDENCY_ADD, addOrChangedDep))
      }
    }
  }

  private def restoreIncoming(incoming: GraphDependency) {
    assert(incoming.getTargetComponent == this)
    val source = incoming.getSourceComponent
    source.removeOutgoingDependency(incoming)
    graphModel.fireChangeEvent(new GraphModelChangeEvent(GraphModelChangeEvent.ChangeType.DEPENDENCY_REMOVE, incoming))
    members.foreach { member =>
      val componentDeps =
        if (source.isInstanceOf[CompositeGraphComponent]) {
          val sourceMembers = source.asInstanceOf[CompositeGraphComponent].getMembers.asScala.toList
          graphModel.findComponentDependencies(sourceMembers, member.asInstanceOf[SimpleGraphComponent])
        }
        else {
          graphModel.findComponentDependencies(List(source), member.asInstanceOf[SimpleGraphComponent])
        }
      componentDeps.foreach { componentDep =>
        val source = graphModel.findById(componentDep.getSourceComponent.getIdentifier)
        val restoredDep = new GraphDependency(source, member, graphModel, componentDep)
        member.addIncomingDependency(restoredDep)
        val addOrChangedDep = source.addOutgoingDependency(restoredDep)
        graphModel.fireChangeEvent(new GraphModelChangeEvent(GraphModelChangeEvent.ChangeType.DEPENDENCY_ADD, addOrChangedDep))
      }
    }
  }

  def addMember(member: GraphComponent) = {   // TODO: try to get rid of this
    members = member +: members
  }

  def getMembers : java.util.List[GraphComponent] = members

  def hasMember(id: String) = ! members.find { _.getIdentifier() == id }.isEmpty

  def getIncomingDependencies(): java.util.List[GraphDependency] = incomingDependencies

  def addIncomingDependency(dependency: GraphDependency): GraphDependency = {
    if (incomingDependencies.contains(dependency)) {
      val existing = incomingDependencies.find { _ == dependency }.get
      existing.merge(dependency)
      return existing
    }
    else {
      incomingDependencies += dependency
      return dependency
    }
  }

  def removeIncomingDependency(dependency: GraphDependency) {
    incomingDependencies = incomingDependencies -= dependency
  }

  def getOutgoingDependencies(): java.util.List[GraphDependency] = outgoingDependencies

  def addOutgoingDependency(dependency: GraphDependency): GraphDependency = {
    if (outgoingDependencies.contains(dependency)) {
      val existing = outgoingDependencies.find { _ == dependency }.get
      existing.merge(dependency)
      return existing
    }
    else {
      outgoingDependencies += dependency
      return dependency
    }
  }

  def removeOutgoingDependency(dependency: GraphDependency) {
    outgoingDependencies = outgoingDependencies -= dependency
  }

  def getLabel: String = {
    val label = getIdentifier.toString
    if (graphModel.labelConverter != null)
      graphModel.labelConverter.convert(label)
    else
      label
  }

  def getIdentifier() = identifier

  override def hashCode(): Int = getIdentifier().hashCode()

  override def equals(other: scala.Any): Boolean = {
    if (other.isInstanceOf[CompositeGraphComponent])
      this.getIdentifier().equals(other.asInstanceOf[CompositeGraphComponent].getIdentifier())
    else
      false
  }

  override def toString() = identifier

  def isVisible() = visible

  def setVisible(visible: Boolean) {
    this.visible = visible;
    if (graphModel.getGraphComponent(identifier) != null) {
    	graphModel.fireChangeEvent(new GraphModelChangeEvent(ChangeType.COMPONENT_VISIBILITY_CHANGE, this))
    }
  }

  @JsonIgnore def getGraphModel() = graphModel

  def setLocation(x: Double, y: Double) {
    this.x = x;
    this.y = y;
    graphModel.fireChangeEvent(new GraphModelChangeEvent(ChangeType.LOCATION_CHANGE, this));
  }

  def getXPosition() = x

  def getYPosition() = y

  def getWidth() = width

  def getHeight() = height

  @Override
  def setBounds(x: Double, y: Double, width: Double, height: Double) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }
  
  def toXml() = {
    <component type={this.getClass().getName()}>
      <identifier>{getIdentifier()}</identifier>
      <visible>{visible}</visible>
      <x>{x}</x>
      <y>{y}</y>
      <width>{width}</width>
      <height>{height}</height>
      <members>
      { for (member <- members) yield 
        {member.toXml()}
      }
      </members>
    </component>
  }

}
