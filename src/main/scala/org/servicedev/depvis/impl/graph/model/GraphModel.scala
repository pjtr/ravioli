/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.impl.graph.model

import org.servicedev.depvis.model._
import scala.collection.mutable.LinkedHashMap
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import java.io.File
import scala.xml.XML
import scala.xml.Node
import org.servicedev.depvis.impl.graph.LabelConverter
import com.lambdaworks.jacks.JacksMapper

/**
 * Actual model for the component graph. Uses a component model as input.
 */
class GraphModel(componentModel: ComponentModel, val labelConverter: LabelConverter = null) {

  var graphComponents: LinkedHashMap[String, GraphComponent] = new LinkedHashMap[String, GraphComponent]
  var modelChangeListeners: List[GraphModelChangeListener] = List()
  var batchOperationActive : Boolean = false
  
  def beginBatchOperation() {
    batchOperationActive = true
  }
  
  def endBatchOperation() {
    batchOperationActive = false
  }

  def prepare() {
    graphComponents clear

    // visit the component model and create a graph model that also includes the incoming dependencies
    val components: List[Component] = componentModel.getComponents().toList

    // Although the following line looks really nice...
    // graphComponents = { components map { component => (component.getIdentifier(), new GraphComponent(this, component)) } toMap }
    // we need the same order as the component model
    components.foreach { component =>
      graphComponents += (component.getIdentifier() -> new SimpleGraphComponent(this, component))
    }

    components.foreach { component =>
      val graphComponent = graphComponents(component.getIdentifier())
      component.getOutgoingDependencies.foreach { dependency =>
        val target = graphComponents(dependency.getTargetComponent.getIdentifier())
        val graphDependency = new GraphDependency(graphComponent, target, this, dependency)
        graphComponent.asInstanceOf[SimpleGraphComponent].addOutgoingDependency(graphDependency);
        // add as incoming dependency on the target component
        target.asInstanceOf[SimpleGraphComponent].addIncomingDependency(graphDependency);
      }
    }
  }

  def getComponentModel() = componentModel

  def getGraphComponents(): java.util.List[GraphComponent] = graphComponents.values.toList

  def getGraphComponent(identifier: String) = {
    if (graphComponents.keySet.contains(identifier)) graphComponents(identifier) else null
  }

  def add(component: GraphComponent) = {
    graphComponents += (component.getIdentifier() -> component);
    fireChangeEvent(new GraphModelChangeEvent(GraphModelChangeEvent.ChangeType.COMPONENT_ADD, component))
  }

  def remove(component: GraphComponent) = {
    graphComponents.remove(component.getIdentifier())
    fireChangeEvent(new GraphModelChangeEvent(GraphModelChangeEvent.ChangeType.COMPONENT_REMOVE, component))
  }

  def containsIdentifier(identifier: String): Boolean = {
    graphComponents.keys.contains(identifier)
  }

  def group(components: java.util.List[GraphComponent], groupId: String): CompositeGraphComponent = {
    group(components.asScala.toList, groupId)
  }

  def group(components: List[GraphComponent], groupId: String): CompositeGraphComponent = {
    if (containsIdentifier(groupId))
      throw new DuplicateNameException()

    val composite = new CompositeGraphComponent(groupId, components, this);
    val firstMember = components.head
    composite.setBounds(firstMember.getXPosition(), firstMember.getYPosition(), firstMember.getWidth(), firstMember.getHeight());
    // remove selected components from the graph
    components.foreach { this.remove(_) }
    // add the composite to the graph
    this.add(composite)
    return composite
  }

  def ungroup(compositeGroup: CompositeGraphComponent): java.util.List[GraphComponent] = {
    // Remove the group
    this.remove(compositeGroup)
    // Add re-add its members
    val members: List[GraphComponent] = compositeGroup.getMembers.toList
    members.foreach {
      this.add(_)
    }
    // Restore the underlying dependencies
    compositeGroup.restoreUnderlyingDependencies
    return members
  }

  def findById(id: String): GraphComponent = {
    graphComponents.get(id).getOrElse({
      // Search all composites
      graphComponents.values.filter { _.isInstanceOf[CompositeGraphComponent] }.find { _.asInstanceOf[CompositeGraphComponent].hasMember(id) }.get
    })
  }

  def findComponentDependencies(from: SimpleGraphComponent, to: List[GraphComponent]): List[Dependency] = {
    to.map { _.asInstanceOf[SimpleGraphComponent].getComponent().getIncomingDependencies.filter { _.getSourceComponent == from.getComponent() } }.flatten
  }

  def findComponentDependencies(from: List[GraphComponent], to: SimpleGraphComponent): List[Dependency] = {
    from.map { _.asInstanceOf[SimpleGraphComponent].getComponent().getOutgoingDependencies.filter { _.getTargetComponent == to.getComponent() } }.flatten
  }

  def save(file: File) {
    XML.save(file.getAbsolutePath(), toXml(), "UTF-8", true)

  }

  // TODO: Also store and fix positions of grouped nodes
  def load(file: File) = {
    // first call prepare() to recreate the graph model since some groups might have been created
    // which will otherwise cause problems when processing the file
    beginBatchOperation()
    prepare()
    def modelNode = XML.loadFile(file)
    def componentNodes = modelNode \\ "component"
    def componentData = componentNodes.map(node => xmlToComponentData(node))
    // first process the simple graph component data
    for (componentData <-  componentData if componentData.isInstanceOf[SimpleGraphComponentData]) {
      componentData.applyData()
    }
    // now process the composite graph components
    for (componentData <-  componentData if componentData.isInstanceOf[CompositeGraphComponentData]) {
      componentData.applyData()
    }
    endBatchOperation()
    fireChangeEvent(new GraphModelChangeEvent(GraphModelChangeEvent.ChangeType.GRAPH_RESET))
    this
  }

  def xmlToComponentData(node: Node) = {
    var component: GraphComponentData = null
    var graphComponentAttributes = DefaultGraphComponentAttributes(node)
    val nodeType = (node \ "@type").text
    if (nodeType == "org.servicedev.depvis.impl.graph.model.SimpleGraphComponent") {
      component = SimpleGraphComponentData(graphComponentAttributes)
    } else if (nodeType == "org.servicedev.depvis.impl.graph.model.CompositeGraphComponent") {
      def members = (node \ "members" \ "component").map { member =>
        // For now we'll assume members will only be simple graph components
        SimpleGraphComponentData(DefaultGraphComponentAttributes(member))
      }
      component = CompositeGraphComponentData(graphComponentAttributes, members)
    }
    component
  }

  def addGraphModelChangeListener(listener: GraphModelChangeListener) {
    modelChangeListeners = modelChangeListeners :+ listener
  }

  def removeGraphModelChangeListener(listener: GraphModelChangeListener) {
    modelChangeListeners = modelChangeListeners.filter { _ != listener }
  }

  private[model] def fireChangeEvent(event: GraphModelChangeEvent) {
    if (!batchOperationActive) {
	    modelChangeListeners.foreach {
	      _.onGraphModelChange(event)
	    }
    }
  }

  private def toXml() = {
    <model version="1.0">{
      graphComponents.values.map { _.asInstanceOf[GraphComponent].toXml() }
    }</model>
  }
  
  def toJSON() : String = {
    JacksMapper.writeValueAsString(graphComponents)
  }
  
  trait GraphComponentAttributes {
    val identifier : String
    val x, y, width, height : Double
    val visible : Boolean
  }
  
  trait GraphComponentData extends GraphComponentAttributes {
    def applyData()
    def applyVisualData(graphComponent : GraphComponent) = {
      graphComponent.setBounds(x, y, width, height)
      graphComponent.setVisible(visible)
    }
    def getIdentifier() = identifier
  }

  class DefaultGraphComponentAttributes(val identifier: String, val x: Double, val y: Double, val width: Double, val height: Double, val visible : Boolean) extends GraphComponentAttributes {
	  
  }

  object DefaultGraphComponentAttributes {
    def apply(node : Node) = {
	    val nodeType = (node \ "@type").text
	    val identifierString = (node \ "identifier").text
	    val xPos = (node \ "x").text.toDouble
	    val yPos = (node \ "y").text.toDouble
	    val width = (node \ "width").text.toDouble
	    val height = (node \ "height").text.toDouble
	    val visible = (node \ "visible").text.toBoolean
	    new DefaultGraphComponentAttributes(identifierString, xPos, yPos, width, height, visible)
    }
  }

  class SimpleGraphComponentData(val identifier: String, val x: Double, val y: Double, val width: Double, val height: Double, val visible : Boolean) extends GraphComponentData {
    def applyData = {
      val component = graphComponents.get(getIdentifier)
      if (component.isDefined) {
        applyVisualData(component.get)
      }
    }
  }
  
  object SimpleGraphComponentData {
    def apply(attributes : GraphComponentAttributes) = {
      new SimpleGraphComponentData(attributes.identifier, attributes.x, attributes.y, attributes.width, attributes.height, attributes.visible)
    }
  }
  
  class CompositeGraphComponentData(val identifier: String, val x: Double, val y: Double, val width: Double, val height: Double, val visible : Boolean, val members: Seq[SimpleGraphComponentData]) extends GraphComponentData {
    override def applyData = {
      // first apply visual data for the members of the group
      for (member <- members) {
        val graphComponent = graphComponents.get(member.getIdentifier)
        if (graphComponent.isDefined) {
        	member.applyVisualData(graphComponent.get)
      	}
      }
      val memberGraphComponents = members.flatMap{member => graphComponents.get(member.getIdentifier)}
      applyVisualData(group(memberGraphComponents, identifier))
    }
  }
  
  object CompositeGraphComponentData {
    def apply(attributes : GraphComponentAttributes, members : Seq[SimpleGraphComponentData]) = {
      new CompositeGraphComponentData(attributes.identifier, attributes.x, attributes.y, attributes.width, attributes.height, attributes.visible, members)
    }
  }

}
