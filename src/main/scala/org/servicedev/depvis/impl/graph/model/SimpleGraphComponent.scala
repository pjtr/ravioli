/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * 
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.impl.graph.model;

import org.servicedev.depvis.impl.graph.model.GraphModelChangeEvent.ChangeType
import org.servicedev.depvis.model.Component
import scala.collection.JavaConversions.seqAsJavaList
import scala.collection.mutable
import scala.xml.XML
import scala.collection.mutable.ListBuffer
import java.util.ArrayList
import java.util.HashMap
import java.util.LinkedHashMap
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonIgnore


/**
 * Graph component implementation. Wraps a component into a graph component.
 *
 */
class SimpleGraphComponent(@JsonIgnore graphModel: GraphModel, @JsonIgnore component: Component) extends GraphComponent {

	private var outgoingDependencies: java.util.Map[GraphDependency, GraphDependency] = new LinkedHashMap[GraphDependency, GraphDependency]()
	private var incomingDependencies: java.util.Map[GraphDependency, GraphDependency] = new LinkedHashMap[GraphDependency, GraphDependency]()

	@JsonProperty("visible") private var visible = true;
	@JsonProperty("x") private var x: Double = 0
	@JsonProperty("y") private var y: Double = 0
	@JsonProperty("width") private var width: Double = 0
	@JsonProperty("height") private var height: Double = 0
	
  def getLabel: String = {
    val label = component.getIdentifier.toString
    if (graphModel.labelConverter != null)
      graphModel.labelConverter.convert(label)
    else
      label
  }

	@JsonProperty("identifier") def getIdentifier() = {
		component.getIdentifier()
	}

  def getComponent() = component

  @JsonProperty("outgoing-dependencies") def getOutgoingDependencies(): java.util.List[GraphDependency] = {
		new ArrayList(outgoingDependencies.keySet())
	}
	
  @JsonProperty("incoming-dependencies") def getIncomingDependencies(): java.util.List[GraphDependency] = {
		new ArrayList(incomingDependencies.keySet())
	}

  def addIncomingDependency(dependency: GraphDependency): GraphDependency = {
    if (incomingDependencies.containsKey(dependency)) {
      val existing = incomingDependencies.get(dependency)
      existing.merge(dependency)
      return existing
    }
    else {
      incomingDependencies.put(dependency, dependency) 
      return dependency
    }
  }

  def addOutgoingDependency(dependency: GraphDependency): GraphDependency = {
    if (outgoingDependencies.containsKey(dependency)) {
      val existing = outgoingDependencies.get(dependency)
      existing.merge(dependency)
      return existing
    }
    else {
      outgoingDependencies.put(dependency, dependency) 
      return dependency
    }
  }

  def removeIncomingDependency(dependency: GraphDependency) {
    incomingDependencies.remove(dependency) 
  }

  def removeOutgoingDependency(dependency: GraphDependency) {
    outgoingDependencies.remove(dependency) 
  }

	override def toString() = {
		getLabel
	}

  override def hashCode(): Int = getIdentifier().hashCode()

  override def equals(other: scala.Any): Boolean = {
    if (other.isInstanceOf[SimpleGraphComponent])
      getIdentifier() == other.asInstanceOf[SimpleGraphComponent].getIdentifier()
    else
      false
  }

  def isVisible() = {
		visible;
	}

	def setVisible(visible: Boolean) {
		this.visible = visible;
		graphModel.fireChangeEvent(new GraphModelChangeEvent(ChangeType.COMPONENT_VISIBILITY_CHANGE, this))
	}
	
	def getGraphModel() = {
		graphModel
	}
	
	def setLocation(x: Double, y: Double) {
		this.x = x;
		this.y = y;
		graphModel.fireChangeEvent(new GraphModelChangeEvent(ChangeType.LOCATION_CHANGE, this));
	}

	@JsonIgnore def getXPosition() = {
		x
	}

	@JsonIgnore def getYPosition() = {
		y
	}

	@JsonIgnore def getWidth() = {
		width
	}

	@JsonIgnore def getHeight() = {
	  height
	}

	@Override
	def setBounds(x: Double, y: Double, width: Double, height: Double) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

  def toXml() = {
    <component type={this.getClass().getName()}>
      <identifier>{getIdentifier()}</identifier>
      <visible>{visible}</visible>
      <x>{x}</x>
      <y>{y}</y>
      <width>{width}</width>
      <height>{height}</height>
    </component>
  }

}
