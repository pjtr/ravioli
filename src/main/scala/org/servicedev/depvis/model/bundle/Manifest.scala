/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * 
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.model.bundle

import scala.collection.mutable.ListBuffer

object Manifest {

  def parseBundleSymbolicNameHeader(header: String): Pair[String, Boolean] = {
    if (header != null) {
      val parts = header.split(';')
      if (parts.size > 1) {
        if (parts(1) == "singleton:=true")
          (parts(0), true)
        else
          (parts(0), false)
      }
      else
        (parts(0), false)
    }
    else
      (null, false)
  }

  def parsePackageHeader(imports: String): Seq[String] = {
    // Find the split positions, everything within quotes ("such"), must be ignored.
    var withinQuotes = false
    val splitPositions: ListBuffer[Int] = ListBuffer()
    for (i <- 0 until imports.length) {
      if (imports(i) == '"') {
        withinQuotes = ! withinQuotes
      }
      else if (imports(i) == ',' && !withinQuotes) {
        splitPositions += i
      }
    }
    // Convert split positions to substring ranges, e.g. (3,7) becomes (-1,3)(7,9) if string length is 10 (-1 compensates for no comma at the beginnen of the string.
    val substringRanges = (-1 +: splitPositions).zip(splitPositions :+ imports.length)
    val packageDeclarations = substringRanges.map { range => imports.substring(range._1 + 1, range._2).trim() }
    packageDeclarations.map { _.takeWhile( c => c != ';') }.filter { ! _.isEmpty }
  }
}
