/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.model.bundle

import org.servicedev.depvis.model.{Dependency, Component}

/**
 * A {code Dependency} that is caused by the source bundle requiring the target bundle by means of a Require-Bundle header.
 * @param sourceComponent
 * @param targetComponent
 */
class BundleDependency(sourceComponent: Component, targetComponent: Component) extends Dependency {

  def getSourceComponent = sourceComponent

  def getTargetComponent = targetComponent

  def getExplanation = targetComponent.getIdentifier
}
