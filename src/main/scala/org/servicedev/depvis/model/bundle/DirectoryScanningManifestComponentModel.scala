/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * 
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.model.bundle;

import java.util.List
import org.servicedev.depvis.model.Component
import java.io._
import scala.collection.JavaConversions.seqAsJavaList
import java.util.jar.Manifest
import scala.Some
import java.util.concurrent.atomic.AtomicInteger
import org.servicedev.depvis.impl.graph.Progress
import scala.collection.immutable
import scala.collection.mutable.ListBuffer

class DirectoryScanningManifestComponentModel(directory: File, progressMonitor: Progress) extends BundleComponentModel {

  val companion = DirectoryScanningManifestComponentModel   // just an alias for readability

  var nonBundleManifests: Seq[String] = immutable.List()
  var invalidManifests: ListBuffer[String] = ListBuffer()

  val components: List[Component] = {
    progressMonitor.setProgressText("Scanning directory...")
    val bundleMetaData: Seq[BundleMetaData] = processFiles(directory, "manifest.mf")
    val (bundles, nonBundles) = bundleMetaData.partition { _.symbolicName != null }
    nonBundleManifests = nonBundles.map { _.fileName }
    createComponents(bundles, progressMonitor)
  }

	override def getComponents(): List[Component] = components

  override def getCreationResult: String = {
    if (! nonBundleManifests.isEmpty || ! invalidManifests.isEmpty) {
      "<html>" +
        (if (!nonBundleManifests.isEmpty)
          "The following manifests don't contain bundle information:<ul>" + (nonBundleManifests.map { file => s"<li>${file}</li>"}).mkString + "</ul>"
        else "") +
        (if (!invalidManifests.isEmpty)
          "The following manifest files are invalid:<ul>" + (invalidManifests.map { file => s"<li>${file}</li>"}).mkString + "</ul>"
        else "") +
      "</html>"
    }
    else null
  }

  def processFiles(dir: File, name: String): Seq[BundleMetaData] = {
    var files = dir.listFiles(new FilenameFilter {
      def accept(dir: File, fileName: String) = fileName.equalsIgnoreCase(name)
    })
    if (files == null)
      files = Array()

    var subdirs = dir.listFiles(new FileFilter {
      def accept(file: File) = file.isDirectory()
    })
    if (subdirs == null)
      subdirs = Array()

    // files in current directory...
    files.map { file =>
      progressMonitor.increment();
      try {
        Some(companion.createMetaData(file))
      } catch {
        case e: Exception => invalidManifests += file.getCanonicalPath(); None
      }
    }.filter { ! _.isEmpty }.map { _.get }.toList ++
      // ... + result from recursive call on subdirectories
      subdirs.toList.map { processFiles( _, name) }.flatten
  }
}

object DirectoryScanningManifestComponentModel {

  val counter = new AtomicInteger()

  def createMetaData(manifestFile: File): BundleMetaData = {
      val manifest = new Manifest(new FileInputStream(manifestFile))
      new BundleMetaData {
        val id = counter.addAndGet(1)

        def fileName = manifestFile.getPath()

        def bundleId: Long = id

        def symbolicName: String = Manifest.parseBundleSymbolicNameHeader(manifest.getMainAttributes().getValue("Bundle-SymbolicName"))._1

        def getHeader(name: String): String = manifest.getMainAttributes().getValue(name)
      }
  }
}
