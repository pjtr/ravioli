/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * 
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.model.bundle

import org.osgi.framework.{Bundle, BundleContext}
import org.servicedev.depvis.model.{Dependency, Component, ComponentModel}
import scala.collection.JavaConversions._
import scala.collection.mutable
import org.servicedev.depvis.impl.graph.Progress
import org.servicedev.depvis.Constants

/**
 * Scans all deployed OSGi bundles and creates a ComponentModel representing the static bundle dependencies (package
 * exports and imports, required bundles).
 * @param allBundles
 * @param progressMonitor
 */
class InContainerBundleScanner(allBundles: Array[Bundle], private val progressMonitor: Progress = null) extends BundleComponentModel {

  def this(bundleContext: BundleContext, progressMonitor: Progress) = this(bundleContext.getBundles, progressMonitor)

  var components: java.util.List[Component] = {
    val bundleMetaData = allBundles.map { bundle =>
      new BundleMetaData {
        def bundleId: Long = bundle.getBundleId

        def symbolicName: String = bundle.getSymbolicName

        def getHeader(name: String): String = bundle.getHeaders.get(name).asInstanceOf[String]

        def fileName: String = bundle.getSymbolicName
      }
    }
    createComponents(bundleMetaData, progressMonitor)
  }

  override def getComponents(): java.util.List[Component] = components
}

class BundleComponent(bundle: BundleMetaData) extends Component {

  private var outgoing: List[Dependency] = List()
  private var incoming: List[Dependency] = List()

  override def getIdentifier: String = bundle.symbolicName

  override def getOutgoingDependencies(): java.util.List[Dependency] = outgoing

  def addOutgoingDependency(dependency: Dependency) {
    outgoing = outgoing :+ dependency
  }

  def setOutgoingDependencies(dependencies: List[Dependency]) {
    outgoing = dependencies
  }

  override def getIncomingDependencies() = incoming

  def addIncomingDependency(dependency: Dependency) {
    incoming = dependency +: incoming
  }

  override def toString: String = bundle.symbolicName

  override def hashCode(): Int = bundle.symbolicName.hashCode()

  override def equals(other: scala.Any): Boolean = getIdentifier.equals(other.asInstanceOf[BundleComponent].getIdentifier)
}