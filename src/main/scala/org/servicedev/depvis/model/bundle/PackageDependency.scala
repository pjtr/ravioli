/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * 
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.model.bundle

import org.servicedev.depvis.model.{Component, Dependency}

/**
 * A {code Dependency} that is caused by the source component using packages provided by the target.
 * @param sourceComponent
 * @param targetComponent
 * @param dependentPackages
 */
class PackageDependency(sourceComponent: Component, targetComponent: Component, var dependentPackages: Set[String]) extends Dependency {

  def getSourceComponent = sourceComponent

  def getTargetComponent = targetComponent

  def getPackages(): Set[String] = dependentPackages

  // Limit access for state modifying methods to classes in current package
  private[bundle] def addPackage(pack: String) {
    dependentPackages += pack
  }

  def getExplanation: Seq[String] = dependentPackages.toList.sortWith( _ < _)
}
