/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.model

import java.util.List

/**
 * The core element of the <code>ComponentModel</code>.
 * The <code>ComponentModel</code> is comprised of components and only components can have dependencies (to other components).
 */
abstract trait Component {
  /**
   * Component identifier
   */
  def getIdentifier(): String

  /**
   * Component's incoming dependencies
   */
  def getIncomingDependencies(): List[Dependency]

  /**
   * Component's outgoing dependencies
   */
  def getOutgoingDependencies(): List[Dependency]
}