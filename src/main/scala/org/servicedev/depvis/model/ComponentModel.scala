/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.model

import java.util.List

/**
 * The model that defines all components and their relationships. Relations between components can be queried
 * using <code>Component.getOutgoingDependencies</code> and <code>Component.getIncomingDependencies</code>.
 */
abstract trait ComponentModel {

  /**
   * Returns all components in the model.
   * @return  all components
   */
  def getComponents(): List[Component]

  /**
   * Returns information about the creation of the model (e.g. invalid components), as an HTML-formatted string.
   * @return  HTML-formatted string or null.
   */
  def getCreationResult(): String = null

  /**
   * Explains a number of dependencies, for human consumption. The returned string should be HTML-formatted.
   * Note that the dependencies can have different source and/or target components, as the caller might be interested
   * in the dependencies between groups of components.
   *
   * @param dependencies  the dependencies to explain
   * @return  explanation or null if the model doesn't provide an explanation
   */
  def explainDependencies(dependencies: Seq[Dependency]): String = null

}