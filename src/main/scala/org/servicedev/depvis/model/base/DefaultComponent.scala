/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.model.base

import java.util.ArrayList
import java.util.List
import org.servicedev.depvis.model.Component
import org.servicedev.depvis.model.Dependency

class DefaultComponent(identifier: String) extends Component {

  private var outgoingDependencies: List[Dependency] = new ArrayList[Dependency]
  private var incomingDependencies: List[Dependency] = new ArrayList[Dependency]

  def getIdentifier: String = identifier

  def getOutgoingDependencies: List[Dependency] = outgoingDependencies

  def getIncomingDependencies: List[Dependency] = incomingDependencies

  def addOutgoingDependency(dependency: Dependency) {
    outgoingDependencies.add(dependency)
  }

  def addIncomingDependency(dependency: Dependency) {
    incomingDependencies.add(dependency)
  }

  override def toString: String = identifier

}