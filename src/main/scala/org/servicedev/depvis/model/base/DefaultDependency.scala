/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.model.base

import org.servicedev.depvis.model.Component
import org.servicedev.depvis.model.Dependency

class DefaultDependency(val sourceComponent: Component, val targetComponent: Component) extends Dependency {

  def getSourceComponent: Component = sourceComponent

  def getTargetComponent: Component = targetComponent

  override def hashCode: Int = {
    return sourceComponent.hashCode + targetComponent.hashCode
  }

  def addSelf(): DefaultDependency = {
    if (sourceComponent.isInstanceOf[DefaultComponent] && targetComponent.isInstanceOf[DefaultComponent]) {
      (sourceComponent.asInstanceOf[DefaultComponent]).addOutgoingDependency(this)
      (targetComponent.asInstanceOf[DefaultComponent]).addIncomingDependency(this)
    }
    return this
  }

  override def equals(other: Any): Boolean = {
    if (other.getClass == this.getClass) {
      val that: DefaultDependency = other.asInstanceOf[DefaultDependency]
      return (this.sourceComponent == that.sourceComponent) && (this.targetComponent == that.targetComponent)
    }
    else return false
  }

}