/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.model.base

import java.util.List
import org.servicedev.depvis.model.{Dependency, Component, ComponentModel}
import java.util
import org.servicedev.depvis.model.bundle.PackageDependency
import scala.collection.immutable

class DefaultComponentModel(components: List[Component]) extends ComponentModel {

  def this() = this(new util.ArrayList())

  def getComponents: List[Component] = components

  def addComponent(component: Component) {
    components.add(component)
  }

  override def explainDependencies(dependencies: Seq[Dependency]): String = {
    dependencies.map { dependency =>
      if (dependency.isInstanceOf[PackageDependency])
        dependency.asInstanceOf[PackageDependency].getExplanation
      else
        immutable.List()
    }.flatten.mkString(",")
  }
}