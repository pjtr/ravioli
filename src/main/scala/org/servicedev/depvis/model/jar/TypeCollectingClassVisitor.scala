/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.model.jar

import org.objectweb.asm._
import scala.collection.mutable

/**
 * An ASM class visitor that will extract all used types from a class.
 */
class TypeCollectingClassVisitor extends ClassVisitor(Opcodes.ASM4) {

  var className: String = null
  val packageTypeMapping = new mutable.HashMap[String, mutable.Set[String]] with mutable.MultiMap[String, String]
  var packageName: String = null

  def getTypes() = {
    packageTypeMapping.keys.filter { pckg => pckg != packageName &&  ! pckg.startsWith("java.") }.toList
  }

  override def visit(version: Int, access: Int, name: String, signature: String, superName: String, interfaces: Array[String]) {
    className = Type.getObjectType(name).getClassName
    packageName = Type.getObjectType(name).getClassName.split('.').init.mkString(".")
    val superType = Type.getObjectType(superName)
    addType(superType)
    interfaces.foreach { i => addType(Type.getObjectType(i)) }
  }

  override def visitField(access: Int, name: String, desc: String, signature: String, value: scala.Any): FieldVisitor = {
    val fieldType = Type.getType(desc)
    addType(fieldType)
    return null
  }

  override def visitMethod(access: Int, name: String, desc: String, signature: String, exceptions: Array[String]): MethodVisitor = {
    val returnType = Type.getReturnType(desc)
    addType(returnType)
    Type.getArgumentTypes(desc).foreach { argType =>
      addType(argType)
    }
    return null
  }

  private def addType(javaType: Type) {
    if (javaType.getSort() == Type.OBJECT) {
      val className = javaType.getClassName()
      val packageName = className.split('.').init.mkString(".")
      packageTypeMapping.addBinding(packageName, className)
    }
  }

}
