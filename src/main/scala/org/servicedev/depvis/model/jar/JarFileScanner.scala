/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.model.jar

import java.io.File
import java.util.zip.ZipFile
import scala.collection.JavaConversions.enumerationAsScalaIterator
import org.objectweb.asm.ClassReader
import org.servicedev.depvis.impl.graph.Progress

class JarFileScanner(jar: File, progress: Progress) extends ByteCodeAnalyzer {

  val zip = new ZipFile(jar)
  progress.setRange(0, zip.size())
  for (entry <- zip.entries()) {
    if (entry.getName().endsWith(".class")) {
      println("processing " + entry.getName())
      // Thread.sleep(100)
      val classInputStream = zip.getInputStream(entry)
      val classReader = new ClassReader(classInputStream)
      classInputStream.close()
      println("Processing " + classReader.getClassName)
      val typeCollector = new TypeCollectingClassVisitor()
      classReader.accept(typeCollector, 0)
      println("Done; " + classReader.getClassName + " uses " + typeCollector.getTypes())
      progress.increment()
      addTypeDependencies(typeCollector.className, typeCollector.getTypes())
    }
  }
}
