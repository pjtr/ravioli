/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.model.jar

import scala.collection.mutable

class ByteCodeAnalyzer {

  // mapping from package to package
  val packageTypeMapping = new mutable.HashMap[String, mutable.Set[String]] with mutable.MultiMap[String, String]

  def allPackages() = {
    packageTypeMapping.keys
  }

  def dependentPackages(pckg: String) = {
    packageTypeMapping(pckg)
  }

  def addTypeDependencies(className: String, usedTypes: List[String]) {
    if (! usedTypes.isEmpty) {
      val fromPackage = packageName(className)
      usedTypes.foreach { toPackage =>
        packageTypeMapping.addBinding(fromPackage, toPackage)
      }
    }
  }

  def packageName(className: String) = className.split('.').init.mkString(".")

}
