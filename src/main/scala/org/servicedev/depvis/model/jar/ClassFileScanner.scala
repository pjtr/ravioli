/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.model.jar

import java.io.{DataInputStream, BufferedInputStream, FileInputStream, File}
import org.objectweb.asm.ClassReader
import org.servicedev.depvis.impl.graph.Progress

class ClassFileScanner(dir: File, progress: Progress) extends ByteCodeAnalyzer {

  var fileCount = 0

  processFiles(dir)

  def processFiles(dir: File) {
    dir.listFiles().foreach { file =>
      if (progress.cancelled()) throw new InterruptedException()
      if (file.isDirectory)
        processFiles(file)
      else
        if (file.getName().contains(".") && file.getName().split('.').last == "class")
          processFile(file)
    }
  }

  def processFile(file: File) {
    progress.setCurrentProgress(fileCount)
    val stream = new BufferedInputStream(new FileInputStream(file))
    try {
      val classReader = new ClassReader(stream)
      val typeCollector = new TypeCollectingClassVisitor()
      classReader.accept(typeCollector, 0)
      addTypeDependencies(typeCollector.className, typeCollector.getTypes())
    }
    catch {
      case _ =>
    }
    finally {
      stream.close()
    }
    fileCount += 1
  }
}
