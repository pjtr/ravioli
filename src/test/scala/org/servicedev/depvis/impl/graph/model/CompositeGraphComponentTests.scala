/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.impl.graph.model

import org.scalatest.{OneInstancePerTest, Matchers, FunSuite}
import org.scalatest.mock.MockitoSugar
import org.servicedev.depvis.model.base.{DefaultComponentModel, DefaultComponent}
import org.servicedev.depvis.model.bundle.PackageDependency
import scala.collection.JavaConverters._
import scala.collection.JavaConversions._

class CompositeGraphComponentTests extends FunSuite with Matchers with MockitoSugar with OneInstancePerTest {

  val compA = new DefaultComponent("A")
  val compB = new DefaultComponent("B")
  val compC = new DefaultComponent("C")
  val compD = new DefaultComponent("D")
  val componentModel = new DefaultComponentModel(List(compA, compB, compC, compD))

  test("composite graph component its outgoing dependencies are collected from members") {
    addDependency(compA, compC, "a->c")
    addDependency(compB, compC, "b->c")

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val (graphCompA, graphCompB, graphCompC, graphCompD) = retrieveGraphComponents(graphModel)

    val compositeAB = graphModel.group(List(graphCompA, graphCompB), "A&B")

    compositeAB.getOutgoingDependencies() should have size 1
    compositeAB.getIncomingDependencies() should have size 0

    val dependency = compositeAB.getOutgoingDependencies().get(0)
    dependency.getSourceComponent shouldBe compositeAB
    dependency.getTargetComponent shouldBe graphCompC

    dependency.getExplanation().split(",") should have size 2
    dependency.getExplanation().split(",") should contain ("a->c")
    dependency.getExplanation().split(",") should contain ("b->c")

    graphCompC.getIncomingDependencies should have size 1
    val incomingDep = graphCompC.getIncomingDependencies.get(0)
    incomingDep.getSourceComponent() shouldBe compositeAB
  }

  test("dependencies to group components are lifted to group level") {
    addDependency(compA, compC, "a->c")
    addDependency(compB, compD, "b->d")

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val (graphCompA, graphCompB, graphCompC, graphCompD) = retrieveGraphComponents(graphModel)

    val compositeCD = graphModel.group(List(graphCompC, graphCompD), "C&D")

    compositeCD.getOutgoingDependencies() should have size 0

    graphCompA.getOutgoingDependencies() should have size 1
    val aOut = graphCompA.getOutgoingDependencies().get(0)
    aOut.getSourceComponent shouldBe graphCompA
    aOut.getTargetComponent shouldBe compositeCD
    aOut.getExplanation().split(",") should have size 1
    aOut.getExplanation().split(",") should contain ("a->c")

    graphCompB.getOutgoingDependencies() should have size 1
    val bOut = graphCompB.getOutgoingDependencies().get(0)
    bOut.getSourceComponent shouldBe graphCompB
    bOut.getTargetComponent shouldBe compositeCD
    bOut.getExplanation().split(",") should have size 1
    bOut.getExplanation().split(",") should contain ("b->d")

    compositeCD.getIncomingDependencies() should have size 2
    val sources = compositeCD.getIncomingDependencies().map { _.getSourceComponent }
    sources should contain (graphCompA)
    sources should contain (graphCompB)
  }

  test("internal dependencies are ignored") {
    addDependency(compA, compC, "a->c")
    addDependency(compB, compD, "b->d")
    addDependency(compC, compD, "c=>d")

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val (graphCompA, graphCompB, graphCompC, graphCompD) = retrieveGraphComponents(graphModel)

    val compositeCD = graphModel.group(List(graphCompC, graphCompD), "C&D")

    compositeCD.getIncomingDependencies() should have size 2
    compositeCD.getOutgoingDependencies() should have size 0
  }

  test("ungroup restores dependencies") {
    addDependency(compA, compC, "a->c")
    addDependency(compB, compD, "b->d")
    addDependency(compC, compD, "c=>d")

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val (graphCompA, graphCompB, graphCompC, graphCompD) = retrieveGraphComponents(graphModel)
    val compositeCD = graphModel.group(List(graphCompC, graphCompD), "C&D")
    graphModel.ungroup(compositeCD)

    graphModel.getGraphComponents() should have size 4
    val (ungroupedA, ungroupedB, ungroupedC, ungroupedD) = retrieveGraphComponents(graphModel)

    ungroupedA.getIncomingDependencies() should have size 0
    ungroupedA.getOutgoingDependencies() should have size 1
    ungroupedA.getOutgoingDependencies().get(0).getSourceComponent shouldBe ungroupedA
    ungroupedA.getOutgoingDependencies().get(0).getTargetComponent shouldBe ungroupedC
    ungroupedA.getOutgoingDependencies().get(0).getExplanation shouldBe "a->c"

    ungroupedB.getIncomingDependencies() should have size 0
    ungroupedB.getOutgoingDependencies() should have size 1
    ungroupedB.getOutgoingDependencies().get(0).getSourceComponent shouldBe ungroupedB
    ungroupedB.getOutgoingDependencies().get(0).getTargetComponent shouldBe ungroupedD
    ungroupedB.getOutgoingDependencies().get(0).getExplanation shouldBe "b->d"

    ungroupedC.getOutgoingDependencies() should have size 1
    ungroupedC.getOutgoingDependencies().get(0).getSourceComponent shouldBe ungroupedC
    ungroupedC.getOutgoingDependencies().get(0).getTargetComponent shouldBe ungroupedD
    ungroupedC.getOutgoingDependencies().get(0).getExplanation shouldBe "c=>d"

    ungroupedC.getIncomingDependencies() should have size 1
    ungroupedC.getIncomingDependencies().get(0).getSourceComponent shouldBe ungroupedA
    ungroupedC.getIncomingDependencies().get(0).getTargetComponent shouldBe ungroupedC
    ungroupedC.getIncomingDependencies().get(0).getExplanation shouldBe "a->c"

    ungroupedD.getOutgoingDependencies() should have size 0
    ungroupedD.getIncomingDependencies() should have size 2
    val bTOd = ungroupedD.getIncomingDependencies().find { _.getSourceComponent == ungroupedB }.get
    bTOd.getTargetComponent shouldBe ungroupedD
    bTOd.getExplanation shouldBe "b->d"
    val cTOd = ungroupedD.getIncomingDependencies().find { _.getSourceComponent == ungroupedC }.get
    cTOd.getTargetComponent shouldBe ungroupedD
    cTOd.getExplanation shouldBe "c=>d"
  }

  test("group refering to group") {
    addDependency(compA, compC, "a->c")
    addDependency(compB, compD, "b->d")

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val (graphCompA, graphCompB, graphCompC, graphCompD) = retrieveGraphComponents(graphModel)

    val compositeAB = graphModel.group(List(graphCompA, graphCompB), "A&B")
    val compositeCD = graphModel.group(List(graphCompC, graphCompD), "C&D")

    compositeAB.getIncomingDependencies() should have size 0
    compositeAB.getOutgoingDependencies() should have size 1
    val ABtoCD = compositeAB.getOutgoingDependencies().get(0)
    ABtoCD.getSourceComponent shouldBe compositeAB
    ABtoCD.getTargetComponent shouldBe compositeCD
    val explanation = ABtoCD.getExplanation
    explanation.split(",") should have size 2
    explanation.split(",") should contain ("a->c")
    explanation.split(",") should contain ("b->d")

    compositeCD.getOutgoingDependencies() should have size 0
    compositeCD.getIncomingDependencies() should have size 1
    val CDfromAB = compositeCD.getIncomingDependencies().get(0)
    CDfromAB.getSourceComponent shouldBe compositeAB
    CDfromAB.getTargetComponent shouldBe compositeCD
    val explanation2 = CDfromAB.getExplanation
    explanation2.split(",") should have size 2
    explanation2.split(",") should contain ("a->c")
    explanation2.split(",") should contain ("b->d")
  }

  test("ungroup group refering to group in reverse order") {
    addDependency(compA, compC, "a->c")
    addDependency(compB, compD, "b->d")

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val (graphCompA, graphCompB, graphCompC, graphCompD) = retrieveGraphComponents(graphModel)

    val compositeCD = graphModel.group(List(graphCompC, graphCompD), "C&D")
    val compositeAB = graphModel.group(List(graphCompA, graphCompB), "A&B")

    // Ungroup AB
    graphModel.ungroup(compositeAB)

    graphModel.getGraphComponents() should have size 3
    graphModel.getGraphComponents() should contain (compositeCD)

    val (ungroupedA, ungroupedB, ungroupedC, ungroupedD) = retrieveGraphComponents(graphModel)

    ungroupedA.getIncomingDependencies should have size 0
    ungroupedA.getOutgoingDependencies should have size 1
    val AtoCD = ungroupedA.getOutgoingDependencies()(0)
    AtoCD.getSourceComponent shouldBe ungroupedA
    AtoCD.getTargetComponent shouldBe compositeCD

    ungroupedB.getIncomingDependencies should have size 0
    ungroupedB.getOutgoingDependencies should have size 1
    val BtoCD = ungroupedB.getOutgoingDependencies()(0)
    BtoCD.getSourceComponent shouldBe ungroupedB
    BtoCD.getTargetComponent shouldBe compositeCD

    ungroupedC shouldBe null
    ungroupedD shouldBe null

    compositeCD.getIncomingDependencies should have size 2
    val aTOcd = compositeCD.getIncomingDependencies().find { _.getSourceComponent == ungroupedA }.get
    aTOcd.getTargetComponent shouldBe compositeCD
    val bTOcd = compositeCD.getIncomingDependencies().find { _.getSourceComponent == ungroupedB }.get
    bTOcd.getTargetComponent shouldBe compositeCD

    // Ungroup CD
    graphModel.ungroup(compositeCD)

    graphModel.getGraphComponents() should have size 4
    val (ungroupedA2, ungroupedB2, ungroupedC2, ungroupedD2) = retrieveGraphComponents(graphModel)

    ungroupedA2.getIncomingDependencies should have size 0
    ungroupedA2.getOutgoingDependencies should have size 1
    val AtoC = ungroupedA2.getOutgoingDependencies()(0)
    AtoC.getSourceComponent shouldBe ungroupedA2
    AtoC.getTargetComponent shouldBe ungroupedC2

    ungroupedB2.getIncomingDependencies should have size 0
    ungroupedB2.getOutgoingDependencies should have size 1
    val BtoD = ungroupedB.getOutgoingDependencies()(0)
    BtoD.getSourceComponent shouldBe ungroupedB2
    BtoD.getTargetComponent shouldBe ungroupedD2

    ungroupedC2.getOutgoingDependencies should have size 0
    ungroupedC2.getIncomingDependencies should have size 1
    ungroupedC2.getIncomingDependencies()(0) shouldBe AtoC

    ungroupedD2.getOutgoingDependencies should have size 0
    ungroupedD2.getIncomingDependencies should have size 1
    ungroupedD2.getIncomingDependencies.get(0) shouldBe BtoD
  }

  test("ungroup group refering to group in same order") {
    addDependency(compA, compC, "a->c")
    addDependency(compB, compD, "b->d")

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val (graphCompA, graphCompB, graphCompC, graphCompD) = retrieveGraphComponents(graphModel)

    val compositeAB = graphModel.group(List(graphCompA, graphCompB), "A&B")
    val compositeCD = graphModel.group(List(graphCompC, graphCompD), "C&D")

    // Ungroup AB
    graphModel.ungroup(compositeAB)

    graphModel.getGraphComponents() should have size 3
    graphModel.getGraphComponents() should contain (compositeCD)
    graphModel.getGraphComponents() should contain (graphCompA)
    graphModel.getGraphComponents() should contain (graphCompB)

    val (ungroupedA, ungroupedB, ungroupedC, ungroupedD) = retrieveGraphComponents(graphModel)

    ungroupedA.getIncomingDependencies should have size 0
    ungroupedA.getOutgoingDependencies should have size 1
    val AtoCD = ungroupedA.getOutgoingDependencies()(0)
    AtoCD.getSourceComponent shouldBe ungroupedA
    AtoCD.getTargetComponent shouldBe compositeCD
    AtoCD.getExplanation() shouldBe "a->c"

    ungroupedB.getIncomingDependencies should have size 0
    ungroupedB.getOutgoingDependencies should have size 1
    val bTOcd = ungroupedB.getOutgoingDependencies()(0)
    bTOcd.getSourceComponent() shouldBe ungroupedB
    bTOcd.getTargetComponent() shouldBe compositeCD
    bTOcd.getExplanation() shouldBe "b->d"

    compositeCD.getOutgoingDependencies() should have size 0
    compositeCD.getIncomingDependencies() should have size 2

    val aTOcd = compositeCD.getIncomingDependencies().find { _.getSourceComponent == ungroupedA }.get
    aTOcd.getSourceComponent() shouldBe ungroupedA
    aTOcd.getTargetComponent() shouldBe compositeCD
    aTOcd.getExplanation() shouldBe "a->c"

    val bToCd = compositeCD.getIncomingDependencies().find { _.getSourceComponent == ungroupedB }.get
    bToCd.getSourceComponent() shouldBe ungroupedB
    bToCd.getTargetComponent() shouldBe compositeCD
    bToCd.getExplanation() shouldBe "b->d"
  }

  test("ungroup restores dependencies whose source (A) is hidden in a composite") {
    addDependency(compA, compC, "a->c")

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val (graphCompA, graphCompB, graphCompC, graphCompD) = retrieveGraphComponents(graphModel)
    val groupC = graphModel.group(List(graphCompC), "CC")
    val groupA = graphModel.group(List(graphCompA), "AA")
    graphModel.ungroup(groupC)

    val (ungroupedA, ungroupedB, ungroupedC, ungroupedD) = retrieveGraphComponents(graphModel)
    groupA.getOutgoingDependencies should have size 1
    groupA.getOutgoingDependencies.get(0).getTargetComponent() shouldBe graphCompC
  }

  test("when restoring dependencies due to ungrouping, dependencies that are the same should be merged to have to right explanation") {
    addDependency(compA, compC, "a->c")
    addDependency(compA, compD, "a->d")
    addDependency(compB, compD, "b->d")

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val (graphCompA, graphCompB, graphCompC, graphCompD) = retrieveGraphComponents(graphModel)
    val compositeAB = graphModel.group(List(graphCompA, graphCompB), "AB")

    val initialAbToD = compositeAB.getOutgoingDependencies().find { _.getTargetComponent == graphCompD }.get
    initialAbToD.getExplanation().split(",") should contain ("a->d")
    initialAbToD.getExplanation().split(",") should contain ("b->d")

    val compositeCD = graphModel.group(List(graphCompC, graphCompD), "CD")
    graphModel.ungroup(compositeCD)

    val abToD = compositeAB.getOutgoingDependencies().find { _.getTargetComponent == graphCompD }.get
    abToD.getExplanation().split(",") should contain ("a->d")
    abToD.getExplanation().split(",") should contain ("b->d")
  }

  test("when restoring dependencies due to ungrouping, incoming dependencies should be merged too") {
    addDependency(compA, compB, "a->b")
    addDependency(compA, compC, "a->c")

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val (graphCompA, graphCompB, graphCompC, graphCompD) = retrieveGraphComponents(graphModel)
    val compositeA  = graphModel.group(List(graphCompA), "AA")
    val compositeBC = graphModel.group(List(graphCompB, graphCompC), "BC")
    graphModel.ungroup(compositeA)

    val ungroupedA = graphModel.getGraphComponents().find { _.getIdentifier == "A" }.get
    ungroupedA.getOutgoingDependencies() should have size 1
    val aToBC = ungroupedA.getOutgoingDependencies()(0)
    aToBC.getExplanation().split(",") should contain ("a->b")
    aToBC.getExplanation().split(",") should contain ("a->c")

    compositeBC.getIncomingDependencies() should have size 1
  }

  test("ungroup restores dependencies from members to others") {
    addDependency(compA, compC, "a->c")
    addDependency(compB, compD, "b->d")
    addDependency(compC, compD, "c=>d")

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val (graphCompA, graphCompB, graphCompC, graphCompD) = retrieveGraphComponents(graphModel)
    val compositeAB = graphModel.group(List(graphCompA, graphCompB), "AB")
    graphModel.ungroup(compositeAB)

    graphModel.getGraphComponents() should have size 4
    val (ungroupedA, ungroupedB, ungroupedC, ungroupedD) = retrieveGraphComponents(graphModel)

    ungroupedA.getOutgoingDependencies should have size 1
    ungroupedA.getOutgoingDependencies()(0).getTargetComponent() shouldBe ungroupedC
    ungroupedA.getOutgoingDependencies()(0).getExplanation() shouldBe "a->c"
    ungroupedB.getOutgoingDependencies should have size 1
    ungroupedB.getOutgoingDependencies()(0).getTargetComponent() shouldBe ungroupedD
    ungroupedB.getOutgoingDependencies()(0).getExplanation() shouldBe "b->d"
  }

  test("after grouping/ungrouping, only single dependencies should remain") {
    addDependency(compA, compC, "a->c")
    addDependency(compB, compD, "b->d")
    addDependency(compC, compD, "c=>d")

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val (graphCompA, graphCompB, graphCompC, graphCompD) = retrieveGraphComponents(graphModel)
    val compositeAB = graphModel.group(List(graphCompA, graphCompB), "AB")
    val compositeCD = graphModel.group(List(graphCompC, graphCompD), "CD")
    graphModel.ungroup(compositeAB)
    graphModel.ungroup(compositeCD)

    val (ungroupedA, ungroupedB, ungroupedC, ungroupedD) = retrieveGraphComponents(graphModel)
    ungroupedA.getOutgoingDependencies should have size 1
    ungroupedB.getOutgoingDependencies should have size 1
    ungroupedC.getIncomingDependencies should have size 1
    ungroupedC.getOutgoingDependencies should have size 1
    ungroupedD.getIncomingDependencies should have size 2
  }

  private def retrieveGraphComponents(graphModel: GraphModel) = {
    val graphComponents = graphModel.getGraphComponents().asScala
    val result = List("A", "B", "C", "D").map { id => graphComponents.find { _.getIdentifier() == id }.getOrElse(null) }
    (result(0), result(1), result(2), result(3))
  }

  private def addDependency(a: DefaultComponent, b: DefaultComponent, reasons: String*) = {
    val dep = new PackageDependency(a, b, Set(reasons:_*))
    a.addOutgoingDependency(dep)
    b.addIncomingDependency(dep)
  }
}
