package org.servicedev.depvis.impl.graph

import org.scalatest.{Matchers, FunSuite}
import scala.collection.JavaConversions._

/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
class RegexBasedLabelConverterTests  extends FunSuite with Matchers {

  test("fixed replacement") {
    val converter = new RegexBasedLabelConverter()
    converter.setRegexReplacements(List("org\\.apache\\.(.*)", "foo"))

    converter.convert("org.apache.felix") shouldBe "foo"
  }

  test("simple regex replacement") {
    val converter = new RegexBasedLabelConverter()
    converter.setRegexReplacements(List("org\\.apache\\.(.*)", "$1"))

    converter.convert("org.apache.felix") shouldBe "felix"
  }

  test("if a regex does not match, the next is used") {
    val converter = new RegexBasedLabelConverter()
    converter.setRegexReplacements(List("this will not match", "mooh!", "org\\.apache\\.(.*)", "$1"))

    converter.convert("org.apache.felix") shouldBe "felix"
  }

  test("too many replacements") {
    val converter = new RegexBasedLabelConverter()
    converter.setRegexReplacements(List("org\\.apache\\.(.*)", "$1$2 up to $12"))

    converter.convert("org.apache.felix") shouldBe "felix$2 up to $12"
  }

  test("funny dollar combinations") {
    val converter = new RegexBasedLabelConverter()
    converter.setRegexReplacements(List("org\\.apache\\.(.*)", "$dollar$$"))

    converter.convert("org.apache.felix") shouldBe "$dollar$$"
  }
}
