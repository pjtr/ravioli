/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.impl.graph.model

import org.scalatest.{Matchers, FunSuite}
import org.scalatest.mock.MockitoSugar
import org.servicedev.depvis.model.base.{DefaultDependency, DefaultComponentModel, DefaultComponent}
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import org.servicedev.depvis.model.bundle.PackageDependency

class GraphModelTests extends FunSuite with Matchers with MockitoSugar {

  test("graph model is created from component model") {
    val compA = new DefaultComponent("A")
    val compB = new DefaultComponent("B")
    val compC = new DefaultComponent("C")

    val aToC = new DefaultDependency(compA, compC)
    compA.addOutgoingDependency(aToC)       // A -> C
    val bToC = new DefaultDependency(compB, compC)
    compB.addOutgoingDependency(bToC)       // B -> C

    val componentModel = new DefaultComponentModel()
    componentModel.addComponent(compA)
    componentModel.addComponent(compB)
    componentModel.addComponent(compC)

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val graphComponents = graphModel.getGraphComponents().asScala
    graphComponents should have size 3

    val graphCompA = graphComponents.find { _.getIdentifier() == "A" }.get
    graphCompA should not be null

    val graphCompB = graphComponents.find { _.getIdentifier() == "B" }.get
    graphCompB should not be null

    val graphCompC = graphComponents.find { _.getIdentifier() == "C" }.get
    graphCompC should not be null

    graphCompA.getIncomingDependencies should have size 0
    graphCompA.getOutgoingDependencies should have size 1

    graphCompB.getIncomingDependencies should have size 0
    graphCompB.getOutgoingDependencies should have size 1

    graphCompC.getIncomingDependencies should have size 2
    graphCompC.getOutgoingDependencies should have size 0
  }

  test("package dependencies are contained in explain") {
    val compA = new DefaultComponent("A")
    val compC = new DefaultComponent("C")

    compA.addOutgoingDependency(new PackageDependency(compA, compC, Set("A->C")))

    val componentModel = new DefaultComponentModel()
    componentModel.addComponent(compA)
    componentModel.addComponent(compC)

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val graphCompA = graphModel.getGraphComponents().asScala.find { _.getIdentifier() == "A" }.get
    graphCompA.getOutgoingDependencies().get(0).getExplanation() shouldEqual "A->C"
  }

  test("find underlying dependencies from component model") {
    val compA = new DefaultComponent("A")
    val compB = new DefaultComponent("B")
    val compC = new DefaultComponent("C")

    val aToB = new DefaultDependency(compA, compB)
    compA.addOutgoingDependency(aToB)       // A -> B
    compB.addIncomingDependency(aToB)
    val aToC = new DefaultDependency(compA, compC)
    compA.addOutgoingDependency(aToC)       // A -> C
    compC.addIncomingDependency(aToC)

    val componentModel = new DefaultComponentModel()
    componentModel.addComponent(compA)
    componentModel.addComponent(compB)
    componentModel.addComponent(compC)

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val graphComponents = graphModel.getGraphComponents().asScala
    val graphCompA = graphComponents.find { _.getIdentifier() == "A" }.get.asInstanceOf[SimpleGraphComponent]
    val graphCompB = graphComponents.find { _.getIdentifier() == "B" }.get.asInstanceOf[SimpleGraphComponent]
    val graphCompC = graphComponents.find { _.getIdentifier() == "C" }.get.asInstanceOf[SimpleGraphComponent]

    val depsAtoB = graphModel.findComponentDependencies(graphCompA, List(graphCompB))
    depsAtoB should have size 1
    depsAtoB should contain (new DefaultDependency(compA, compB))

    val depsAtoBC = graphModel.findComponentDependencies(graphCompA, List(graphCompB, graphCompC))
    depsAtoBC should have size 2
    depsAtoBC should contain (new DefaultDependency(compA, compB))
    depsAtoBC should contain (new DefaultDependency(compA, compC))
  }

  test("find underlying dependencies from several components to one") {
    val compA = new DefaultComponent("A")
    val compB = new DefaultComponent("B")
    val compC = new DefaultComponent("C")
    val compD = new DefaultComponent("D")

    val aToC = new DefaultDependency(compA, compC)
    compA.addOutgoingDependency(aToC)       // A -> C
    compC.addIncomingDependency(aToC)
    val bToC = new DefaultDependency(compB, compC)
    compA.addOutgoingDependency(bToC)       // B -> C
    compC.addIncomingDependency(bToC)
    val aToD = new DefaultDependency(compA, compD)
    compA.addOutgoingDependency(aToD)
    compD.addIncomingDependency(aToD)

    val componentModel = new DefaultComponentModel()
    componentModel.addComponent(compA)
    componentModel.addComponent(compB)
    componentModel.addComponent(compC)
    componentModel.addComponent(compD)

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val graphComponents = graphModel.getGraphComponents().asScala
    val graphCompA = graphComponents.find { _.getIdentifier() == "A" }.get.asInstanceOf[SimpleGraphComponent]
    val graphCompB = graphComponents.find { _.getIdentifier() == "B" }.get.asInstanceOf[SimpleGraphComponent]
    val graphCompC = graphComponents.find { _.getIdentifier() == "C" }.get.asInstanceOf[SimpleGraphComponent]
    val graphCompD = graphComponents.find { _.getIdentifier() == "D" }.get.asInstanceOf[SimpleGraphComponent]

    val depsABtoC = graphModel.findComponentDependencies(List(graphCompA, graphCompB), graphCompC)

    depsABtoC should have size 2
    depsABtoC should contain (new DefaultDependency(compA, compC))
    depsABtoC should contain (new DefaultDependency(compB, compC))

    val depsABtoD = graphModel.findComponentDependencies(List(graphCompA, graphCompB), graphCompD)

    depsABtoD should have size 1
    depsABtoD should contain (new DefaultDependency(compA, compD))
  }

  test("find component that is hidden in a composite") {
    val compA = new DefaultComponent("A")
    val compB = new DefaultComponent("B")
    val componentModel = new DefaultComponentModel()
    componentModel.addComponent(compA)
    componentModel.addComponent(compB)

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val graphComponents = graphModel.getGraphComponents().asScala
    val graphCompA = graphComponents.find { _.getIdentifier() == "A" }.get.asInstanceOf[SimpleGraphComponent]
    val graphCompB = graphComponents.find { _.getIdentifier() == "B" }.get.asInstanceOf[SimpleGraphComponent]
    graphModel.group(List(graphCompA, graphCompB), "group")

    val componentFound = graphModel.findById("A")

  }
  
    def createSimpleXMLNode = {
    <component type="org.servicedev.depvis.impl.graph.model.SimpleGraphComponent">
		<identifier>A</identifier>
		<visible>true</visible>
		<x>15.812616274974175</x>
		<y>621.0892891532263</y>
		<width>140.0</width>
		<height>30.0</height>
		</component>
  }
  
  def createCompositeXMLNode = {
    <component type="org.servicedev.depvis.impl.graph.model.CompositeGraphComponent">
		<identifier>groepje</identifier>
		<visible>true</visible>
		<x>508.38152349933955</x>
		<y>252.86131750515875</y>
		<width>140.0</width>
		<height>30.0</height>
		<members>
    <component type="org.servicedev.depvis.impl.graph.model.SimpleGraphComponent">
      <identifier>A 1</identifier>
      <visible>true</visible>
      <x>239.58933482389438</x>
      <y>369.11672722658926</y>
      <width>140.0</width>
      <height>30.0</height>
    </component><component type="org.servicedev.depvis.impl.graph.model.SimpleGraphComponent">
      <identifier>A 2</identifier>
      <visible>true</visible>
      <x>299.36255668223055</x>
      <y>205.93685115604666</y>
      <width>140.0</width>
      <height>30.0</height>
    </component><component type="org.servicedev.depvis.impl.graph.model.SimpleGraphComponent">
      <identifier>A 3</identifier>
      <visible>true</visible>
      <x>166.13533910119554</x>
      <y>122.38902951703278</y>
      <width>140.0</width>
      <height>30.0</height>
    </component>
		</members>
	</component>
  }
  
  test("Simple graph components are correctly read from xml") {
    val componentModel = new DefaultComponentModel()
    val graphModel = new GraphModel(componentModel)
    val node = 	<component type="org.servicedev.depvis.impl.graph.model.SimpleGraphComponent">
		<identifier>identifier</identifier>
		<visible>true</visible>
		<x>15.812616274974175</x>
		<y>621.0892891532263</y>
		<width>140.0</width>
		<height>30.0</height>
		</component>
    val componentData = graphModel.xmlToComponentData(node)
    componentData.isInstanceOf[GraphModel#SimpleGraphComponentData] shouldEqual true
    componentData.identifier shouldEqual "identifier"
    componentData.x shouldEqual 15.812616274974175
    componentData.y shouldEqual 621.0892891532263
    componentData.width shouldEqual 140.0
    componentData.height shouldEqual 30.0
    componentData.visible shouldEqual true
  }
  
  test("Composite graph components are correctly read from xml") {
    val componentModel = new DefaultComponentModel()
    val graphModel = new GraphModel(componentModel)
    val node = 	createCompositeXMLNode
    val componentData = graphModel.xmlToComponentData(node)
    componentData.isInstanceOf[GraphModel#CompositeGraphComponentData] shouldEqual true
    componentData.identifier shouldEqual "groepje"
    componentData.x shouldEqual 508.38152349933955
    componentData.y shouldEqual 252.86131750515875
    componentData.width shouldEqual 140.0
    componentData.height shouldEqual 30.0
    componentData.visible shouldEqual true
    componentData.asInstanceOf[GraphModel#CompositeGraphComponentData].members should have size 3
  }
  
  test("Simple graph component layout from XML is correctly applied to the model") {
    val componentModel = new DefaultComponentModel()
    val compA = new DefaultComponent("A")
    componentModel.addComponent(compA)
    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()
    val node = 	createSimpleXMLNode
    val componentData = graphModel.xmlToComponentData(node)
    componentData.applyData()
    val graphComponent = graphModel.getGraphComponent(componentData.getIdentifier)
    graphComponent.getXPosition shouldEqual 15.812616274974175
    graphComponent.getYPosition shouldEqual 621.0892891532263
    graphComponent.getWidth shouldEqual 140.0
    graphComponent.getHeight shouldEqual 30.0
  }
  
  test("Composite graph component layout read from XML is correctly applied to model") {
    val componentModel = new DefaultComponentModel()
    val compA1 = new DefaultComponent("A 1")
    val compA2 = new DefaultComponent("A 2")
    val a3Identifier = "A 3"
    val compA3 = new DefaultComponent(a3Identifier)
    componentModel.addComponent(compA1)
    componentModel.addComponent(compA2)
    componentModel.addComponent(compA3)
    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()
    val node = 	createCompositeXMLNode
    val componentData = graphModel.xmlToComponentData(node)
    componentData.isInstanceOf[GraphModel#CompositeGraphComponentData] shouldEqual true
    componentData.asInstanceOf[GraphModel#CompositeGraphComponentData].members should have size 3
    componentData.applyData()
    graphModel.getGraphComponents should have size 1
    graphModel.ungroup(graphModel.getGraphComponent(componentData.getIdentifier).asInstanceOf[CompositeGraphComponent])
    graphModel.getGraphComponents should have size 3
    val a3GraphComponent = graphModel.getGraphComponent(a3Identifier)
    a3GraphComponent.getXPosition() shouldEqual 166.13533910119554
    a3GraphComponent.getYPosition() shouldEqual 122.38902951703278
    a3GraphComponent.getWidth() shouldEqual 140.0
    a3GraphComponent.getHeight() shouldEqual 30.0
  }
  
  test("graphmodel is correctly serialized to JSON") {
    val componentModel = new DefaultComponentModel()
    val compA = new DefaultComponent("A")
    val compB = new DefaultComponent("B")
    val compC = new DefaultComponent("C")
    val compD = new DefaultComponent("D")
    val compE = new DefaultComponent("E")
    componentModel.addComponent(compA)
    componentModel.addComponent(compB)
    componentModel.addComponent(compC)
    componentModel.addComponent(compD)
    componentModel.addComponent(compE)
    val aToC = new DefaultDependency(compA, compC)
    compA.addOutgoingDependency(aToC)       
    compC.addIncomingDependency(aToC)
    val aToD = new DefaultDependency(compA, compD)
    compA.addOutgoingDependency(aToD)       
    compD.addIncomingDependency(aToD)
    val eToB = new DefaultDependency(compE, compB)
    compE.addOutgoingDependency(eToB)
    compB.addIncomingDependency(eToB)
    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()
    // group D and E
    graphModel.group(List(graphModel.getGraphComponent("D"), graphModel.getGraphComponent("E")), "group")
    val expectedResult = """{"A":{"visible":true,"x":0.0,"y":0.0,"width":0.0,"height":0.0,"identifier":"A","label":"A","outgoing-dependencies":[{"explanation":"","source":"A","target":"C"},{"explanation":"","source":"A","target":"group"}],"incoming-dependencies":[]},"B":{"visible":true,"x":0.0,"y":0.0,"width":0.0,"height":0.0,"identifier":"B","label":"B","outgoing-dependencies":[],"incoming-dependencies":[{"explanation":"","source":"group","target":"B"}]},"C":{"visible":true,"x":0.0,"y":0.0,"width":0.0,"height":0.0,"identifier":"C","label":"C","outgoing-dependencies":[],"incoming-dependencies":[{"explanation":"","source":"A","target":"C"}]},"group":{"members":[{"visible":true,"x":0.0,"y":0.0,"width":0.0,"height":0.0,"identifier":"D","label":"D","outgoing-dependencies":[],"incoming-dependencies":[]},{"visible":true,"x":0.0,"y":0.0,"width":0.0,"height":0.0,"identifier":"E","label":"E","outgoing-dependencies":[],"incoming-dependencies":[]}],"visible":true,"width":0.0,"height":0.0,"identifier":"group","incomingDependencies":[{"explanation":"","source":"A","target":"group"}],"outgoingDependencies":[{"explanation":"","source":"group","target":"B"}],"xposition":0.0,"yposition":0.0,"label":"group"}}"""
    graphModel.toJSON shouldEqual expectedResult
  }

  test("creating a group with a duplicate name should be impossible") {
    val compA = new DefaultComponent("A")
    val compB = new DefaultComponent("B")
    val componentModel = new DefaultComponentModel(List(compA, compB))

    val graphModel = new GraphModel(componentModel)
    graphModel.prepare()

    val graphComponents = graphModel.getGraphComponents().asScala
    val graphCompA = graphComponents.find { _.getIdentifier() == "A" }.get.asInstanceOf[SimpleGraphComponent]
    val graphCompB = graphComponents.find { _.getIdentifier() == "B" }.get.asInstanceOf[SimpleGraphComponent]
    intercept[DuplicateNameException] {
      graphModel.group(List(graphCompA, graphCompB), "A")
    }
  }
}
