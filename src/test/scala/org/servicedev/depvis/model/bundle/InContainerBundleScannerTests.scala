/**
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * 
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.depvis.model.bundle

import org.scalatest.FunSuite
import org.scalatest.Matchers
import org.osgi.framework.Bundle
import org.mockito.Mockito._
import java.util.{Hashtable, Dictionary}
import scala.collection.JavaConversions._
import org.mockito.stubbing.Answer
import org.mockito.invocation.InvocationOnMock
import org.scalatest.mock.MockitoSugar

class InContainerBundleScannerTests extends FunSuite with Matchers with MockitoSugar {

  test("one bundle") {
    val systemBundle = mockBundleWithExportsAndImports(0, "some.export")

    val components = new InContainerBundleScanner(Array(systemBundle)).getComponents()

    components should have size 1
    components.get(0).getOutgoingDependencies() should have size 0
  }

  test("bundle should not have dependency to self") {
    val systemBundle = mockBundleWithExportsAndImports(0, "my.package", "my.package")

    val components = new InContainerBundleScanner(Array(systemBundle)).getComponents()

    components should have size 1
    components.get(0).getOutgoingDependencies() should have size 0
  }

  test("bundle should not have dependency for package it exports itself") {
    val bundleA = mockBundleWithExportsAndImports(1, "my.package", "my.package")
    val bundleB = mockBundleWithExportsAndImports(2, "my.package")

    val components = new InContainerBundleScanner(Array(bundleA, bundleB)).getComponents()

    components should have size 2
    val compA = components.find { _.getIdentifier() == "bundle 1" }.get
    compA.getOutgoingDependencies() should have size 0
  }

  test("bundle A uses Bundle B") {
    val bundleA = mockBundleWithExportsAndImports(1, "a.a", "b.b")
    val bundleB = mockBundleWithExportsAndImports(2, "b.b")

    val components = new InContainerBundleScanner(Array(bundleA, bundleB)).getComponents()
    components should have size 2

    val compA = components.find { _.getIdentifier() == "bundle 1" }.get
    val compB = components.find { _.getIdentifier() == "bundle 2" }.get

    compA.getIncomingDependencies should have size 0
    compA.getOutgoingDependencies() should have size 1
    compA.getOutgoingDependencies()(0).getSourceComponent() shouldEqual compA
    compA.getOutgoingDependencies()(0).getTargetComponent() shouldEqual compB

    compB.getOutgoingDependencies() should have size 0
    compB.getIncomingDependencies should have size 1
    compB.getIncomingDependencies()(0).getSourceComponent shouldBe compA
    compB.getIncomingDependencies()(0).getTargetComponent shouldBe compB
  }

  test("bundle A imports one package from bundle B its package list") {
    val bundleA = mockBundleWithExportsAndImports(1, "a.a", "b.b")
    val bundleB = mockBundleWithExportsAndImports(2, "b.b1, b.b2, b.b, b.b3, b.b4")

    val components = new InContainerBundleScanner(Array(bundleA, bundleB)).getComponents()
    components should have size 2

    val compA = components.find { _.getIdentifier() == "bundle 1" }.get
    val compB = components.find { _.getIdentifier() == "bundle 2" }.get

    compA.getOutgoingDependencies() should have size 1
    compA.getOutgoingDependencies()(0).getSourceComponent() shouldEqual compA
    compA.getOutgoingDependencies()(0).getTargetComponent() shouldEqual compB

    compB.getOutgoingDependencies() should have size 0
  }

  test("deployed jar without OSGi headers causes no problems") {
    val nonBundleJar = mock[Bundle]
    when(nonBundleJar.getHeaders()).thenAnswer(new Answer[Dictionary[String,String]]{
      def answer(invocation: InvocationOnMock): Dictionary[String, String] = new Hashtable()
    })
  }

  test("bundle A depends on bundle B for multiple packages") {
    val bundleA = mockBundleWithExportsAndImports(1, "a.a", "b.b2,b.b4,b.b5")
    val bundleB = mockBundleWithExportsAndImports(2, "b.b1,b.b2,b.b3,b.b4,b.b5,b.b6,b.b7")

    val components = new InContainerBundleScanner(Array(bundleA, bundleB)).getComponents()
    components should have size 2

    val compA = components.find { _.getIdentifier() == "bundle 1" }.get
    compA.getOutgoingDependencies() should have size 1
    compA.getOutgoingDependencies()(0) shouldBe a [PackageDependency]

    val dependency: PackageDependency = compA.getOutgoingDependencies()(0).asInstanceOf[PackageDependency]
    dependency.getPackages should have size 3
    dependency.getPackages should contain ("b.b2")
    dependency.getPackages should contain ("b.b4")
    dependency.getPackages should contain ("b.b5")
  }

  test("minimize dependencies") {
    val bundleA = mockBundleWithExportsAndImports(1, "very.common")
    val bundleB = mockBundleWithExportsAndImports(2, "very.common, b.b")
    val bundleC = mockBundleWithExportsAndImports(3, "", "very.common, b.b")

    val components = new InContainerBundleScanner(Array(bundleA, bundleB, bundleC)).getComponents()

    val compC = components.find { _.getIdentifier() == "bundle 3" }.get
    compC.getOutgoingDependencies should have size 1
    val packages = compC.getOutgoingDependencies()(0).asInstanceOf[PackageDependency].getPackages()
    packages should contain ("very.common")
    packages should contain ("b.b")
  }

  test("require bundle leads to dependency too") {
    val bundleA = mockBundleWithExportsAndImports(1, "very.common")
    val bundleB = mockBundleWithRequireBundle(2, "bundle 1")

    val components = new InContainerBundleScanner(Array(bundleA, bundleB)).getComponents()

    val compB = components.find { _.getIdentifier() == "bundle 2" }.get
    compB.getOutgoingDependencies should have size 1
    compB.getOutgoingDependencies.get(0) shouldBe a [BundleDependency]

    val compA = components.find { _.getIdentifier() == "bundle 1" }.get
    compA.getIncomingDependencies should have size 1
    compA.getIncomingDependencies.get(0) shouldBe a [BundleDependency]
  }

  test("bundle component model explains both package and bundle dependencies") {
    val bundleA = mockBundleWithExportsAndImports(1, "very.common")
    val bundleB = mockBundleWithExportsAndImportsAndRequireBundle(2, "bundle 1", "very.common", "c.c")
    val bundleC = mockBundleWithExportsAndImports(3, "c.c")

    val componentModel = new InContainerBundleScanner(Array(bundleA, bundleB, bundleC))

    val compB = componentModel.getComponents().find { _.getIdentifier() == "bundle 2" }.get
    val explanation = componentModel.explainDependencies(compB.getOutgoingDependencies())

    explanation should include ("c.c")
    explanation should include ("bundle 1")
  }

  test("explanation for set of dependencies does not contain duplicates") {
    val bundleA = mockBundleWithExportsAndImportsAndRequireBundle(1, "bundle 3", "a.a", "c.c")
    val bundleB = mockBundleWithExportsAndImportsAndRequireBundle(2, "bundle 3", "b.b", "c.c")
    val bundleC = mockBundleWithExportsAndImports(3, "c.c")
    val componentModel = new InContainerBundleScanner(Array(bundleA, bundleB, bundleC))

    val compC = componentModel.getComponents().find { _.getIdentifier() == "bundle 3" }.get
    val dependenciesOnC = compC.getIncomingDependencies()

    val explanation = componentModel.explainDependencies(dependenciesOnC)
    println(explanation)
    explanation.split("<br/>").filter { _.contains("c.c") } should have size 1
    explanation.split("<br/>").filter { _.contains("bundle 3") } should have size 1
  }

  def mockBundleWithExportsAndImports(bundleId: Long, exports: String, imports: String = ""): Bundle = {
    val bundle = mock[Bundle]
    val headers: Dictionary[String, String] = new Hashtable(Map(
      "Export-Package" -> exports,
      "Import-Package" -> imports))
    when(bundle.getHeaders()).thenAnswer(new Answer[Dictionary[String,String]]{
      def answer(invocation: InvocationOnMock): Dictionary[String, String] = headers
    })
    when(bundle.getBundleId()).thenReturn(bundleId)
    when(bundle.getSymbolicName()).thenReturn("bundle " + bundleId)
    bundle
  }

  def mockBundleWithExportsAndImportsAndRequireBundle(bundleId: Long, required: String, exports: String, imports: String = ""): Bundle = {
    val bundle = mock[Bundle]
    val headers: Dictionary[String, String] = new Hashtable(Map(
      "Require-Bundle" -> required,
      "Export-Package" -> exports,
      "Import-Package" -> imports))
    when(bundle.getHeaders()).thenAnswer(new Answer[Dictionary[String,String]]{
      def answer(invocation: InvocationOnMock): Dictionary[String, String] = headers
    })
    when(bundle.getBundleId()).thenReturn(bundleId)
    when(bundle.getSymbolicName()).thenReturn("bundle " + bundleId)
    bundle
  }

  def mockBundleWithRequireBundle(bundleId: Long, required: String): Bundle = {
    val bundle = mock[Bundle]
    val headers: Dictionary[String, String] = new Hashtable(Map(
      "Require-Bundle" -> required))
    when(bundle.getHeaders()).thenAnswer(new Answer[Dictionary[String,String]]{
      def answer(invocation: InvocationOnMock): Dictionary[String, String] = headers
    })
    when(bundle.getBundleId()).thenReturn(bundleId)
    when(bundle.getSymbolicName()).thenReturn("bundle " + bundleId)
    bundle
  }
}
