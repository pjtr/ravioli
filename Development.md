# Ravioli development

## Language and environment

The tool is written in a combination of Scala and Java. Scala is mostly used for the business logic (computing dependencies etc.) and Java for the UI. It is likely that more Java code is moved to Scala in the near future. 

If you are not familiar with Scala, we highly recommend the book [Scala for the impatient](http://horstmann.com/scala/); Scala is so much different from Java that learning by doing can be a very frustrating task, that might lead to the impression that Scala is just too complex or too hard to learn; reading the book will give a much more gentle and pleasant introduction in the Scala domain. The Scala version used is 2.10.3.

The build is based on gradle. Development can be done both in Eclipse and Intellij; the Eclipse project files are in the git repository, and for Intellij you can simply generate them by typing `gradle idea`. 

Test are based on [ScalaTest](http://www.scalatest.org/); we use [Mockito](https://code.google.com/p/mockito/) for mocking.

Deployment format is of course an OSGi bundle.

### References

* Scala [API documentation](http://www.scala-lang.org/api/current/#package)
* [Scala Test](http://www.scalatest.org/)
* Mockito [documentation](http://docs.mockito.googlecode.com/hg/latest/org/mockito/Mockito.html)
* JGraph [API](http://api.globis.ethz.ch/jgraph/index.html)
* OSGi [API](http://www.osgi.org/javadoc/r4v43/core/index.html?org/osgi/framework/Bundle.html)
* Gradle [User guide](http://www.gradle.org/docs/current/userguide/userguide.html)


## Core interfaces

The central interface is `ComponentModel`. It represents the component-graph to analyse in an abstract way: the model is comprised of `Component` objects and `Component` objects have dependencies on each other that are expressed by `Dependency`. That's pretty much it.


## Building

The gradle properties file specifies a larger MaxPermSize than the default, which is necessary to build and test succesfully. Because using non-standard Java VM settings, we use the gradle daemon to reduce (gradle) startup time. However, the daemon will periodically run out of PermGen space anyway and exit with an OutOfMemoryError. Just restart the build and gradle will launch a new daemon. If you don't like this behavior, you can run gradle without the daemon with `--no-daemon` option, e.g.: `gradle --no-daemon assemble`.


## Contribute

You are welcome to contribute! Just fork the git repository on BitBucket and start coding! See the [Issue list](https://bitbucket.org/uiterlix/ravioli/issues?status=new&status=open) on BitBucket for inspiration, or check the [Todo list](https://bitbucket.org/uiterlix/dependencyvisualise/wiki/Home) on the wiki.

If you want to send us a pull request, please commit your changes on a separate branch.

## Feedback

We would like to hear from you. Whether you are enthousiastic about the tool, or you think it sucks, we would like to know why. So why not drop us a mail at gmail? Xander (dot) Uiterlinden or Peter (dot) Doornbosch.
